//
//  CardDetails.m
//  RoadyoDispatch
//
//  Created by 3Embed on 18/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "CardDetails.h"

@implementation CardDetails

@dynamic expMonth;
@dynamic expYear;
@dynamic idCard;
@dynamic last4;
@dynamic cardtype;

@end
