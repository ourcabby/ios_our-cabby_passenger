//
//  ListViewCell.m
//  privMD
//
//  Created by Rahul Sharma on 19/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "FavouriteDriverViewCell.h"
#import <Canvas/CSAnimationView.h>

@implementation FavouriteDriverViewCell

@synthesize docImage;
@synthesize docDistance;
@synthesize starRatingView;
@synthesize docName;
@synthesize activityIndicator;
@synthesize animationView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
        animationView = [[CSAnimationView alloc] initWithFrame:CGRectMake(0, 0, 320, 92)];
        animationView.backgroundColor = [UIColor whiteColor];
        animationView.duration = 0.2;
        animationView.delay    = 0;
        animationView.type     = CSAnimationTypeZoomOut;
        [self.contentView addSubview:animationView];

        //section 1
        docImage = [[RoundedImageView alloc]initWithFrame:CGRectMake(10,13,65, 65)];
        docImage.image = [UIImage imageNamed:@"doctor_image_thumbnail.png"];
        [animationView addSubview:docImage];
        
        docName = [[UILabel alloc]initWithFrame:CGRectMake(85, 4, 230, 30)];
        [Helper setToLabel:docName Text:@"" WithFont:HELVETICANEUE_LIGHT FSize:18 Color:[UIColor blackColor]];
        [animationView addSubview:docName];
        
        
        starRatingView = [[AXRatingView alloc] initWithFrame:CGRectMake(85, docName.frame.size.height,125,25)];
        starRatingView.stepInterval = 0.0;
        starRatingView.highlightColor = [UIColor colorWithRed:0.044 green:0.741 blue:1.000 alpha:1.000];
        starRatingView.userInteractionEnabled = NO;
        [starRatingView sizeToFit];
        
        [animationView addSubview:starRatingView];
        
        docDistance = [[UILabel alloc]initWithFrame:CGRectMake(85, starRatingView.frame.origin.y+starRatingView.frame.size.height, 200, 30)];
        [Helper setToLabel:docDistance Text:@"" WithFont:HELVETICANEUE_LIGHT FSize:18 Color:[UIColor blackColor]];
        [animationView addSubview:docDistance];
        
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(50-20/2, 60-20/2, 20, 20)];
        [self.docImage addSubview:activityIndicator];
        // activityIndicator.backgroundColor=[UIColor greenColor];
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
