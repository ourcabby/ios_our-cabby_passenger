//
//  PaymentViewController.m
//  PQ
//
//  Created by Rahul Patil on 01/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PaymentViewController.h"
#import "XDKAirMenuController.h"
#import "PaymentDetailsViewController.h"
#import "Database.h"
#import "CardDetails.h"
#import "CustomNavigationBar.h"
#import "PatientViewController.h"
#import "PaymentTableViewCell.h"
#import "WebViewController.h"
#import "User.h"
#import "RPCustomIOSAlertView.h"
#import "CardLoginViewController.h"

@interface PaymentViewController ()<CustomNavigationBarDelegate>
{
    float keyboardHeight;
    UITapGestureRecognizer *tap;
    PaymentTableViewCell *cell;
    NSInteger defaultCardIndex;
    NSString *amountToAdd;
}

@property(strong,nonatomic) UIButton *addPaymentbutton;
@property (strong,nonatomic)  UITextField *activeTextField;

@end

@implementation PaymentViewController
@synthesize addPaymentbutton;
@synthesize paymentTableView;
@synthesize callback;
@synthesize addMoneytextField, addMoneyBtn1, addMoneyBtn2, addMoneyBtn3;
@synthesize walletView, walletHeadingView, walletHeadingLabel;
@synthesize currentBalanceStatusLabel, currentBalanceAmountLabel;
@synthesize activeTextField;
@synthesize amountToSettle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

#pragma mark - ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addCustomNavigationBar];
    amountToAdd = @"0";
    addMoneyBtn1.titleLabel.numberOfLines = 0;
    addMoneyBtn2.titleLabel.numberOfLines = 0;
    addMoneyBtn3.titleLabel.numberOfLines = 0;
    self.addMoneyView.layer.borderWidth = 1;
    self.addMoneyView.layer.cornerRadius = 5;
    self.addMoneyView.layer.masksToBounds = YES;
    self.addMoneyView.layer.borderColor = UIColorFromRGB(0x08B4E8).CGColor;
    [self tableViewReloadAndAdjustUI];
    
    currencyStr=[PatientGetLocalCurrency getCurrencyLocal:[@"" floatValue]];
    self.addMoneytextField.placeholder = currencyStr;
    
    NSString *str = NSLocalizedString(@"100", @"100");
    [self.addMoneyBtn1 setTitle:[PatientGetLocalCurrency getCurrencyLocal:[str floatValue]] forState:UIControlStateNormal];
    str= NSLocalizedString(@"200",@"200");
    [self.addMoneyBtn2 setTitle:[PatientGetLocalCurrency getCurrencyLocal:[str floatValue]] forState:UIControlStateNormal];
    str= NSLocalizedString(@"300",@"300");
    [self.addMoneyBtn3 setTitle:[PatientGetLocalCurrency getCurrencyLocal:[str floatValue]] forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    [self getWalletCurrentMoney];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (_isComingFromMapVC == YES)
    {
        self.addMoneytextField.userInteractionEnabled = NO;
        addMoneyBtn1.userInteractionEnabled = NO;
        addMoneyBtn2.userInteractionEnabled = NO;
        addMoneyBtn3.userInteractionEnabled = NO;
        self.addMoneytextField.text = amountToSettle;
        self.currentBalanceAmountLabel.text = [NSString stringWithFormat:@"- %@", amountToSettle];
    }
    else
    {
        self.addMoneytextField.userInteractionEnabled = YES;
        addMoneyBtn1.userInteractionEnabled = YES;
        addMoneyBtn2.userInteractionEnabled = YES;
        addMoneyBtn3.userInteractionEnabled = YES;
    }
    appDelegate = (PatientAppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate = (PatientAppDelegate*)[UIApplication sharedApplication].delegate;
    context = [appDelegate managedObjectContext];
    if (context!=nil)
    {
        arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
    }
    [self tableViewReloadAndAdjustUI];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITapGestureRecognizer Delagate -
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)cardDetailsButtonClicked:(id)sender {
    UIButton *mBtn = (UIButton *)sender;
    isGoingDelete = YES;
    _dict = _arrayContainingCardInfo[mBtn.tag];
    [self performSegueWithIdentifier:@"goToPaymentDetail" sender:self];
}

-(void)cardDetailsClicked:(NSDictionary *)getDict {
    _dict =  [getDict mutableCopy];
    [self performSegueWithIdentifier:@"goToPaymentDetail" sender:self];
}

-(void)addPayment {
    [self performSegueWithIdentifier:@"goTocardScanController" sender:self];
//    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"webView"];
//    webView.title = NSLocalizedString(@"Add Card", @"Add Card");
//    NSString *url = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserPaymentURL];
//    webView.weburl = url;
//    webView.isComingFromVC = 2;
//    [self.navigationController pushViewController:webView animated:YES];
}

-(void) getCardDetailsFromService {
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable]) {
        [self sendServiceToGetCardDetail];
    } else {
        [[ProgressIndicator sharedInstance] showMessage:kNetworkErrormessage On:self.view];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"goToPaymentDetail"]) {
        PaymentDetailsViewController *PDVC = [segue destinationViewController];
        PDVC.containingDetailsOfCard = _dict;
        PDVC.callback = ^() {
            [self sendServiceToDeleteCard:_dict[@"id"]];
        };
    }
    else if([segue.identifier isEqualToString:@"goTocardScanController"])
    {
        CardLoginViewController *CLVC = [segue destinationViewController];
        CLVC.isComingFromPayment = 1;
    }
    else if ([segue.identifier isEqualToString:@"walletHistoryVC"])
    {
        
    }
}

- (void)menuButtonPressedAccount {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

-(void)cancelButtonPressedAccount {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"MY WALLET", @"MY WALLET")];
    if(_isComingFromMapVC == YES) {
        [customNavigationBarView setLeftBarButtonTitle:@""];
        [customNavigationBarView hideLeftMenuButton:YES];
        [customNavigationBarView setleftBarButtonImage:[UIImage imageNamed:@"payment_cancel_btn_on"] :[UIImage imageNamed:@"payment_cancel_btn_off"]];
    }
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    if(_isComingFromMapVC == YES)
        [self cancelButtonPressedAccount];
    else
        [self menuButtonPressedAccount];
}

-(void)tableViewReloadAndAdjustUI
{
    [paymentTableView reloadData];
    CGFloat screenWidth = self.view.frame.size.width;
    CGFloat screenHeight = self.view.frame.size.height - 44;
    
    self.mainScrollView.frame = CGRectMake(0, 44, screenWidth, screenHeight);
    CGRect frame = walletView.frame;
    
    if (_isComingFromMapVC == YES && amountToSettle.length == 0)
    {
        paymentTableView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
        frame.origin.y = screenHeight;
    }
    else
    {
        if (paymentTableView.contentSize.height < screenHeight - walletView.frame.size.height) {
            paymentTableView.frame = CGRectMake(0, 0, screenWidth, paymentTableView.contentSize.height);
            frame.origin.y = paymentTableView.frame.size.height;
        } else {
            paymentTableView.frame = CGRectMake(0, 0, screenWidth, screenHeight - walletView.frame.size.height - 20);
            frame.origin.y = screenHeight - walletView.frame.size.height - 20;
        }
    }
    frame.origin.x = 0;
    frame.size.width = screenWidth;
    frame.size.height = walletView.frame.size.height;
    walletView.frame = frame;
    self.mainScrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(walletView.frame) + 10);
}

#pragma mark - WebService call

-(void)sendServiceToGetCardDetail {
    NSString *deviceId;
    if (IS_SIMULATOR)
        deviceId = kPMDTestDeviceidKey;
    else
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    NSString *sesstionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *pToken = @"";
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    else
        pToken = @"";
    
    NSDictionary *params = @{@"ent_sess_token": sesstionToken,
                             @"ent_dev_id": deviceId,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getCards"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [self getCardDetails:response];
                                   }
                               }];
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
}

-(void)getCardDetails:(NSDictionary *)response {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response)
    {
        return;
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([dictResponse[@"errFlag"] intValue] == 1 && ([dictResponse[@"errNum"] intValue] == 6 || [dictResponse[@"errNum"] intValue] == 7))
        {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [XDKAirMenuController relese];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
            User *user = [[User alloc] init];
            [user deleteUserSavedData];
        }
        else  if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            _arrayContainingCardInfo = dictResponse[@"cards"];
            [self addInDataBase];
            arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
            [self tableViewReloadAndAdjustUI];
            NSString *currentBal = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(dictResponse[@"walletBal"]) floatValue]];
            [addMoneytextField resignFirstResponder];
            currentBalanceAmountLabel.text = [NSString stringWithFormat:@"%@",currentBal];
            if ([dictResponse[@"walletBal"] integerValue] <= 0) {
                [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0xeb1313)];
            } else {
                [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0x666666)];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
            [self tableViewReloadAndAdjustUI];

        }
        else if([response[@"errFlag"] integerValue] == 1 && [response[@"errNum"] integerValue] == 51)
        {
            NSString *currentBal = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(dictResponse[@"walletBal"]) floatValue]];
            [addMoneytextField resignFirstResponder];
            currentBalanceAmountLabel.text = [NSString stringWithFormat:@"%@",currentBal];
            if ([dictResponse[@"walletBal"] integerValue] <= 0) {
                [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0xeb1313)];
            } else {
                [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0x666666)];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
            [self tableViewReloadAndAdjustUI];
        }
    }
}

-(void)sendServiceToDeleteCard:(NSString *)cardID
{
    NSString *deviceId;
    if (IS_SIMULATOR)
        deviceId = kPMDTestDeviceidKey;
    else
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    NSString *sesstionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSDictionary *params = @{@"ent_sess_token": sesstionToken,
                             @"ent_dev_id": deviceId,
                             @"ent_cc_id":cardID,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"removeCard"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success) {
                                       [self getDeleteCardResponse:response cardIDToDelete:cardID];
                                   }
                               }];
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
}

-(void)getDeleteCardResponse:(NSDictionary *)response cardIDToDelete:(NSString *)cardID {
    if (!response) {
        [[ProgressIndicator sharedInstance]  hideProgressIndicator];
    } else {
        [[ProgressIndicator sharedInstance]  hideProgressIndicator];
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0) {
            [Database DeleteCard:cardID];
            arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
            [self tableViewReloadAndAdjustUI];
        } else {
            [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
        }
    }
}

-(void) getWalletCurrentMoney {
    NSString *sid = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUSID]);
    NSDictionary *params = @{
                             @"ent_sid":flStrForStr(sid),
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"GetWalletMoney"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getWalletCurrentMoneyResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Checking Current Balance...", @"Checking Current Balance...")];
}

-(void) getWalletCurrentMoneyResponse:(NSDictionary *) responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil) {
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [XDKAirMenuController relese];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDict[@"errMsg"]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
        User *user = [[User alloc] init];
        [user deleteUserSavedData];
    } else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    } else {
        NSString *currentWalletAmount = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(responseDict[@"amount"]) floatValue]];
        [[NSUserDefaults standardUserDefaults] setObject:currentWalletAmount forKey:kNSUWalletAmount];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        currentBalanceAmountLabel.text = [NSString stringWithFormat:@"%@",currentWalletAmount];
        if ([responseDict[@"amount"] integerValue] <= 0) {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:Lato_Bold FSize:12 Color:UIColorFromRGB(0x000000)];
        } else {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:Lato_Bold FSize:12 Color:UIColorFromRGB(0x000000)];
        }
    }
}


-(void) addMoneyToWallet:(NSString *)amount withCardID:(NSString *)cardID {
    NSUserDefaults *udPlotting = [NSUserDefaults standardUserDefaults];
    NSString *sessionToken = [udPlotting objectForKey:KDAcheckUserSessionToken];
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
        deviceID = kPMDTestDeviceidKey;
    else
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_amount":flStrForStr(amount),
                             @"ent_token": flStrForStr(cardID),
                             @"ent_date_time":[Helper getCurrentNetworkDateTime]
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"AddMoneyToWallet"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self addMoneyToWalletResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Adding Money...", @"Adding Money...")];
}

-(void) addMoneyToWalletResponse:(NSDictionary *) responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil){
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [XDKAirMenuController relese];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDict[@"errMsg"]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
        User *user = [[User alloc] init];
        [user deleteUserSavedData];
    } else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        [addMoneyBtn1 setSelected: NO];
        [addMoneyBtn2 setSelected: NO];
        [addMoneyBtn3 setSelected: NO];
        NSString *currentBal = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(responseDict[@"amount"]) floatValue]];
        addMoneytextField.text = @"";
        currentBalanceAmountLabel.text = [NSString stringWithFormat:@"%@",currentBal];
        [[NSUserDefaults standardUserDefaults] setObject:currentBal forKey:kNSUWalletAmount];
        if ([responseDict[@"amount"] integerValue] <= 0)
        {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0xeb1313)];
        }
        else
        {
            [Helper setToLabel:currentBalanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:Lato_Bold FSize:14 Color:UIColorFromRGB(0x666666)];
        }
        if (_isComingFromMapVC)
        {
            if (callback)
            {
                [self cancelButtonPressedAccount];
                callback(@"Wallet Money Added",responseDict[@"amount"]);
            }
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
        }
    }
}


#pragma mark -UITABLEVIEW DELEGATE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (kPMDPaymentType) {
        return arrDBResult.count + 1;
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 45)];
    headerView.backgroundColor = UIColorFromRGB(0xF1F2F7);
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 7.5, [UIScreen mainScreen].bounds.size.width-50, 30)];
    [Helper setToLabel:headerLabel Text:NSLocalizedString(@"SAVED CARDS", @"SAVED CARDS") WithFont:Lato_Bold FSize:10 Color:UIColorFromRGB(0x222328)];
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kLangaugeCodeOtherThanEnglish] && kRTLSupport)
        headerLabel.textAlignment = NSTextAlignmentRight;
    else
        headerLabel.textAlignment = NSTextAlignmentLeft;
    
    [headerView addSubview:headerLabel];
    return headerView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"paymentTableViewCell";
    cell = (PaymentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell == nil) {
        cell =[[PaymentTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if(indexPath.row == arrDBResult.count) {
        [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Add Credit Card", @"Add Credit Card") WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x666666)];
        cell.iconImageView.image = [UIImage imageNamed:@"card_icon"];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"help_next_arrow_icon_off"]];
        cell.itemLabel.frame = CGRectMake(50, 7.5, cell.frame.size.width - 50 - 50, 30);
        cell.iconImageView.frame = CGRectMake(10, 7.5, 30, 30);
        cell.lineView.frame = CGRectMake(0, cell.frame.size.height-1, cell.frame.size.width, 1);
        if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kLangaugeCodeOtherThanEnglish] && kRTLSupport)
            cell.textLabel.textAlignment = NSTextAlignmentRight;
        else
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
//    else if (indexPath.row == arrDBResult.count) {
//        [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Cash", @"Cash") WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x666666)];
//        cell.iconImageView.image = [UIImage imageNamed:@"new_cash_icon"];
//        cell.accessoryView = nil;
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        cell.itemLabel.frame = CGRectMake(50, 8, cell.frame.size.width - 50 - 20, 30);
//        NSInteger paymentType = [[NSUserDefaults standardUserDefaults] integerForKey:kNSUDefaultpaymentType];
//        if (paymentType == 2 ||  (paymentType == 0 && !arrDBResult.count)){
//            defaultCardIndex = indexPath;
//        }
//    }
    else {
        CardDetails *card = arrDBResult[indexPath.row];
        NSString *last4 = [card.last4 substringFromIndex: [card.last4 length] - 4];
        NSString *findRangeForString = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Ends with", @"Ends with"), last4];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:findRangeForString attributes:nil];
        NSRange range1 = [findRangeForString rangeOfString:NSLocalizedString(@"Ends with", @"Ends with")];
        NSRange range2 = [findRangeForString rangeOfString: last4];
        [attributedString addAttribute: NSForegroundColorAttributeName
                          value: UIColorFromRGB(0xA1A6BB)
                          range: range1];
        [attributedString addAttribute: NSFontAttributeName
                          value:  [UIFont fontWithName:Lato_Bold size:10]
                          range: range1];
        
        [attributedString addAttribute: NSForegroundColorAttributeName
                                 value: UIColorFromRGB(0x000000)
                                 range: range2];
        
        [attributedString addAttribute: NSFontAttributeName
                                 value:  [UIFont fontWithName:Lato_Bold size:10]
                                 range: range2];

        cell.itemLabel.attributedText = attributedString;
        cell.iconImageView.image = [self setPlaceholderToCardType:card.cardtype];
        cell.itemLabel.frame = CGRectMake(50, 7.5, cell.frame.size.width - 50 - 50, 30);
        cell.iconImageView.frame = CGRectMake(10, 7.5, 30, 30);
        cell.lineView.frame = CGRectMake(0, cell.frame.size.height-1, cell.frame.size.width, 1);
        if ([card.isDefault integerValue] == 1)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"new_correct_icon"]];
            defaultCardIndex = indexPath.row + 1;
        }
        else
        {
            cell.accessoryView = nil;
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == arrDBResult.count)
    {
        [self addPayment];
    }
    else
    {
        cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        CardDetails *card;
        NSString *cardID;
        cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"new_correct_icon"]];
        if (defaultCardIndex > 0 && indexPath.row != defaultCardIndex - 1)
        {
            card = arrDBResult[defaultCardIndex - 1];
            cardID = card.idCard;
            [Database updateCardDetailsForCardId:cardID andStatus:@"0"];
            NSIndexPath *defaultIndex = [NSIndexPath indexPathForRow:defaultCardIndex inSection:0];
            [self tableView:paymentTableView didDeselectRowAtIndexPath:defaultIndex];
        }
        defaultCardIndex = indexPath.row + 1;
        card = arrDBResult[defaultCardIndex - 1];
        cardID = card.idCard;
        [Database updateCardDetailsForCardId:cardID andStatus:@"1"];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    cell = [tableView cellForRowAtIndexPath:indexPath];
    if(indexPath.row == arrDBResult.count) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"help_next_arrow_icon_off"]];
    } else {
        cell.accessoryView = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == arrDBResult.count) {
        return NO;
    } else {
        return YES;
    }
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        if (indexPath.row == defaultCardIndex - 1) {
            defaultCardIndex = 0;
        }
        CardDetails *card = arrDBResult[indexPath.row];
        [self sendServiceToDeleteCard:card.idCard];
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    return @[deleteAction];
}

-(UIImage *)setPlaceholderToCardType:(NSString *)mycardType{
    NSString* cardTypeName;
    if([mycardType isEqualToString:@"amex"])
        cardTypeName = @"stp_card_amex";
    else if([mycardType isEqualToString:@"diners"])
        cardTypeName = @"stp_card_diners";
    else if([mycardType isEqualToString:@"discover"])
        cardTypeName = @"stp_card_discover";
    else if([mycardType isEqualToString:@"jcb"])
        cardTypeName = @"stp_card_jcb";
    else if([mycardType isEqualToString:@"masterCard"])
        cardTypeName = @"stp_card_mastercard";
    else if([mycardType isEqualToString:@"Visa"])
        cardTypeName = @"stp_card_visa";
    else
        cardTypeName = @"stp_card_placeholder_template";
    return [UIImage imageNamed:cardTypeName];
}


-(void)addInDataBase {
    NSString *defaultCardID;
    arrDBResult = [[Database getCardDetails] mutableCopy];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
    NSArray *filterdArray = [[arrDBResult filteredArrayUsingPredicate:predicate] mutableCopy];
    if (filterdArray.count) {
        CardDetails *card = filterdArray[0];
        defaultCardID = card.idCard;
    }
    [Database DeleteAllCard];
    [arrDBResult removeAllObjects];
    [_arrayContainingCardInfo enumerateObjectsUsingBlock:^(id dict, NSUInteger index, BOOL *stop) {
        Database *db = [[Database alloc] init];
        NSMutableDictionary *cardDict = [dict mutableCopy];
        if ((defaultCardID.length && [cardDict[@"id"] isEqualToString:defaultCardID]) || (!defaultCardID.length && index == 0)) {
            [cardDict setObject:@"1" forKey:@"isDefault"];
        } else {
            [cardDict setObject:@"0" forKey:@"isDefault"];
        }
        [db makeDataBaseEntry:cardDict];
        if (index >= _arrayContainingCardInfo.count) {
            *stop = YES;
        }
    }];
}

#pragma mark - UIAlertView

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1 && buttonIndex == 1)
    {
    }
}



#pragma mark - UIButton Actions -

- (IBAction)openHistoryAction:(id)sender
{
    [self.view endEditing:YES];
    [self performSegueWithIdentifier:@"walletHistoryVC" sender:self];
}

- (IBAction)addMoneyBtn1Action:(id)sender
{
    NSString *str = NSLocalizedString(@"100", @"100");
    addMoneytextField.text = [NSString stringWithFormat:@"%@",[PatientGetLocalCurrency getCurrencyLocal:[str floatValue]]];
    [self performSelector:@selector(setSelectedButton:) withObject:addMoneyBtn1 afterDelay:0];
}

- (IBAction)addMoneyBtn2Action:(id)sender
{
    NSString *str = NSLocalizedString(@"200", @"200");
    addMoneytextField.text = [NSString stringWithFormat:@"%@",[PatientGetLocalCurrency getCurrencyLocal:[str floatValue]]];
   
    [self performSelector:@selector(setSelectedButton:) withObject:addMoneyBtn2 afterDelay:0];
}

- (IBAction)addMoneyBtn3Action:(id)sender
{
    NSString *str = NSLocalizedString(@"300", @"300");
    addMoneytextField.text = [NSString stringWithFormat:@"%@",[PatientGetLocalCurrency getCurrencyLocal:[str floatValue]]];
 
    [self performSelector:@selector(setSelectedButton:) withObject:addMoneyBtn3 afterDelay:0];
}


-(void) setSelectedButton:(UIButton *)sender {
    if ([sender isEqual:addMoneyBtn1]) {
        [addMoneyBtn1 setSelected: YES];
        [addMoneyBtn2 setSelected: NO];
        [addMoneyBtn3 setSelected: NO];
    } else if([sender isEqual:addMoneyBtn2]) {
        [addMoneyBtn1 setSelected: NO];
        [addMoneyBtn2 setSelected: YES];
        [addMoneyBtn3 setSelected: NO];
    } else if ([sender isEqual:addMoneyBtn3]) {
        [addMoneyBtn1 setSelected: NO];
        [addMoneyBtn2 setSelected: NO];
        [addMoneyBtn3 setSelected: YES];
    }
}

- (IBAction)addCreditMoneyBtnAction:(id)sender
{
    NSString *textFiledText = flStrForObj(addMoneytextField.text);
    NSString *amount = @"";
    NSRange replaceRange = [textFiledText rangeOfString:currencyStr];
    
    if (replaceRange.location != NSNotFound)
    {
        amount = [textFiledText stringByReplacingCharactersInRange:replaceRange withString:@""];
    }
    else if(textFiledText.length>0)
    {
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        [nf setNumberStyle:NSNumberFormatterCurrencyStyle];
        double d = [[nf numberFromString:textFiledText] doubleValue];
    }
    
    
    NSInteger number = [amount integerValue];
    if (number == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"First enter amount"];
    }
    else if(number < 100 && !_isComingFromMapVC) {
        [Helper showAlertWithTitle:@"Message" Message:[NSString stringWithFormat:@"%@%@%@",@"The amount need to be more than",currencyStr,@"99"]];
    }
    else if(!arrDBResult.count)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Please add your credit card first and then try again."];
    }
    else {
        [addMoneytextField resignFirstResponder];
        amountToAdd = amount;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
        NSArray *filterdArray = [[arrDBResult filteredArrayUsingPredicate:predicate] mutableCopy];
        if (filterdArray.count) {

            UIView *containerView = [Helper createViewWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Are you sure that do you want to do transaction with above selected card?", @"Are you sure that do you want to do transaction with above selected card?")];
            RPCustomIOSAlertView *alertView = [[RPCustomIOSAlertView alloc] init];
            [alertView setContainerView:containerView];
            [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Change",@"Change"), NSLocalizedString(@"Confirm", @"Confirm"), nil]];
            [alertView setOnButtonTouchUpInside:^(RPCustomIOSAlertView *alertView, int buttonIndex) {
                if (buttonIndex == 1) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDefault CONTAINS[cd] %@",@"1"];
                    NSArray *filterdArray = [[arrDBResult filteredArrayUsingPredicate:predicate] mutableCopy];
                    CardDetails *card = filterdArray[0];
                    [self addMoneyToWallet:amountToAdd withCardID:(NSString *)card.idCard];
                }
                [alertView close];
            }];
            [alertView setUseMotionEffects:true];
            [alertView show];
        } else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:@"Please selected your credit card first and then try again."];
        }
    }
}


#pragma mark - TextFields

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    activeTextField = textField;
    [addMoneyBtn1 setSelected: NO];
    [addMoneyBtn2 setSelected: NO];
    [addMoneyBtn3 setSelected: NO];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [textField becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField.text length] > 0) {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ([reachability isNetworkAvailable]) {
            if ([textField isEqual:addMoneytextField]) {
            }
        } else {
            [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"No Network", @"No Network") On:self.view];
        }
    }
    self.activeTextField = nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *typedText = textField.text;
    typedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (typedText.length <= currencyStr.length)
    {
        textField.text = currencyStr;
        return NO;
    }
    else if (typedText.length >= currencyStr.length+7)
    {
        [textField resignFirstResponder];
        return NO;
    }
    if (typedText.length)
    {
        NSString *amount;
        NSRange replaceRange = [typedText rangeOfString:currencyStr];
        if (replaceRange.location != NSNotFound)
        {
            amount = [typedText stringByReplacingCharactersInRange:replaceRange withString:@""];
        }
        switch ([amount integerValue])
        {
            case 499:
                [addMoneyBtn1 setSelected:YES];
                break;
            case 999:
                [addMoneyBtn2 setSelected:YES];
                break;
            case 1999:
                [addMoneyBtn3 setSelected:YES];
                break;
            default:
                [addMoneyBtn1 setSelected: NO];
                [addMoneyBtn2 setSelected: NO];
                [addMoneyBtn3 setSelected: NO];
                break;
        }
    }
    return YES;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/*--------------------------------------------*/
#pragma -marks Keyboard Appear and Hide Methods
/*--------------------------------------------*/
/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification {
    tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.walletView addGestureRecognizer:tap];
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:keyboardHeight];
}

/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification {
    [self.walletView removeGestureRecognizer:tap];
    [self moveViewDown];
}

/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height {
    float textfieldMaxY = CGRectGetMaxY(textfield.frame) + 50;
    UIView *view = [textfield superview];
    while (view != [self.view superview]) {
        textfieldMaxY += CGRectGetMinY(view.frame);
        view = [view superview];
    }
    float remainder = CGRectGetHeight(self.view.frame) - (textfieldMaxY + height);
    if (remainder >= 0) {
        return;
    }
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.mainScrollView.contentOffset = CGPointMake(0, -remainder);
                     }];
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown {
    self.mainScrollView.contentOffset = CGPointMake(0, -20);
    [self tableViewReloadAndAdjustUI];
}


@end
