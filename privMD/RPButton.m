//
//  RPButton.m
//  RoadyoDispatch
//
//  Created by 3Embed on 06/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "RPButton.h"

@implementation RPButton


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
//        [self customInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
//        [self customInit];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    [self customInit];
}

- (void)setNeedsLayout {
    [super setNeedsLayout];
    [self setNeedsDisplay];
}

- (void)prepareForInterfaceBuilder {
    [self customInit];
}

- (void)customInit
{
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = self.cornerRadius;
    self.layer.borderWidth = self.borderWidth;
    self.layer.borderColor = self.borderColor.CGColor;
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if (highlighted)
    {
        self.backgroundColor = self.highLightedColor;
    }
    else
    {
        self.backgroundColor = self.normalColor;
    }
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    if (selected)
    {
        self.backgroundColor = self.selectedColor;
    }
    else
    {
        self.backgroundColor = self.normalColor;
    }
}


@end
