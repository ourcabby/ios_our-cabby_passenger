//
//  walletHistoryTableViewCell.m
//  RoadyoDispatch
//
//  Created by 3Embed on 23/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "walletHistoryTableViewCell.h"

@implementation walletHistoryTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)updateCellUI:(NSDictionary *)dict {
    NSString *balanceAmount = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(dict[@"amount"]) floatValue]];
    if ([flStrForStr(dict[@"type"]) integerValue] == 1) {
        self.paymentStatusImageView.image = [UIImage imageNamed:@"down_arrow_icon"];
        self.walletBalanceLabel.text = [NSString stringWithFormat:@"%@ %@",balanceAmount, NSLocalizedString(@"OUR CABBY Money Balance", @"OUR CABBY Money Balance")];
        self.paymentReasonLabel.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"OUR CABBY Money Recharge: ", @"OUR CABBY Money Recharge: "), flStrForStr(dict[@"txn_id"])];
    } else {
        self.paymentStatusImageView.image = [UIImage imageNamed:@"up_arrow_icon"];
        self.walletBalanceLabel.text = [NSString stringWithFormat:@"%@ %@",balanceAmount, NSLocalizedString(@"OUR CABBY Money Balance", @"OUR CABBY Money Balance")];
        self.paymentReasonLabel.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"OUR CABBY Booking", @"OUR CABBY Booking")];
    }
    self.dateLabel.text = [self getDate:flStrForStr(dict[@"date"])];
    self.amountLable.text = balanceAmount;
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    self.paymentStatusImageView.frame = CGRectMake(5, 10, 20, 20);
    self.paymentReasonLabel.frame = CGRectMake(25, 10, screenWidth-25-95, 20);
    self.amountLable.frame = CGRectMake(screenWidth-25-95, 10, 95, 20);
    self.walletBalanceLabel.frame = CGRectMake(25, 31, screenWidth-25, 20);
    self.dateLabel.frame = CGRectMake(25, 55, screenWidth-25, 20);
}

-(NSString*)getDate:(NSString *)aDatefromServer {
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EE, d LLLL yyyy hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
}

@end
