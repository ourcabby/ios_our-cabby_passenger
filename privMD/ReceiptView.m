//
//  ReceiptView.m
//  RoadyoDispatch
//

//  Created by 3Embed on 25/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ReceiptView.h"
#import "ReceiptCell.h"
#import "Receipt2TabelViewCell.h"

@implementation ReceiptView

@synthesize onCompletion;

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"ReceiptView" owner:self options:nil] firstObject];
    UINib *cellNib = [UINib nibWithNibName:@"ReceiptCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ReceiptCell"];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ReceiptCell2"];
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (invoiceData == nil)
        return 0;
    else if (invoiceData && [invoiceData[@"subTotal"] floatValue]<[invoiceData[@"min_fare"] floatValue])
    {
        if([invoiceData[@"walletDeducted"] integerValue] == 0)
        {
            return 12;
        }
        else
        {
            return 13;
        }
    }
    else
    {
        if([invoiceData[@"walletDeducted"] integerValue] == 0)
        {
            return 11;
        }
        else
        {
            return 12;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *cellArray = [[NSBundle mainBundle] loadNibNamed:@"ReceiptCell" owner:self options:nil];
    if (invoiceData && [invoiceData[@"subTotal"] floatValue]<[invoiceData[@"min_fare"] floatValue])
    {
        if (indexPath.row == 1)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Minimum Fare", @"Minimum Fare") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *minimumFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"min_fare"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:minimumFee WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
       }
      else if (indexPath.row == 0)
      {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Base Fare",@"Base Fare") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *baseFee = [PatientGetLocalCurrency getCurrencyLocal:[flStrForStr(invoiceData[@"baseFee"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:baseFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden=YES;
            return cell;
      }
      else if (indexPath.row == 2)
      {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Distance Fare",@"Distance Fare") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *distanceFee = [PatientGetLocalCurrency getCurrencyLocal:[flStrForStr(invoiceData[@"distanceFee"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:distanceFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden=YES;
            return cell;
       }
       else if (indexPath.row == 3)
       {
           ReceiptCell *cell = [cellArray firstObject];
           [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Time Fare",@"Time Fare") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
           NSString *distanceFee = [PatientGetLocalCurrency getCurrencyLocal:[flStrForStr(invoiceData[@"timeFee"]) floatValue]];
           [Helper setToLabel:cell.priceLabel Text:distanceFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
           cell.lineLabel.hidden=YES;
           return cell;
       }
       else if (indexPath.row == 4)
       {
           ReceiptCell *cell = [cellArray firstObject];
           [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Airport Fare",@"Airport Fare") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
           NSString *distanceFee = [PatientGetLocalCurrency getCurrencyLocal:[flStrForStr(invoiceData[@"airportFee"]) floatValue]];
           [Helper setToLabel:cell.priceLabel Text:distanceFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
           cell.lineLabel.hidden=YES;
           return cell;
       }
        else if (indexPath.row == 5)
        {
            ReceiptCell *cell = [cellArray firstObject];
            NSString *discountLabel = NSLocalizedString(@"Discount", @"Discount");
            if([invoiceData[@"code"] length])
            {
                if ([invoiceData[@"discountType"] integerValue] == 1)
                {
                    discountLabel = [NSString stringWithFormat:@"%@ ( %@ ) ( %@ %% )",discountLabel, flStrForStr(invoiceData[@"code"]), flStrForStr(invoiceData[@"discountVal"])];
                }
                else if ([invoiceData[@"discountType"] integerValue] == 2)
                {
                    NSString *discountWithCurrency = [PatientGetLocalCurrency getCurrencyLocal:[flStrForStr(invoiceData[@"discountVal"]) floatValue]];
                    discountLabel = [NSString stringWithFormat:@"%@ ( %@ ) ( %@ )", discountLabel, flStrForStr(invoiceData[@"code"]), discountWithCurrency];
                }
            }
            [Helper setToLabel:cell.itemLabel Text:discountLabel WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *discountFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"discount"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:discountFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = NO;
            return cell;
        }
        else if (indexPath.row == 7)
        {
            ReceiptCell *cell = [cellArray firstObject];
            NSString *tip = NSLocalizedString(@"Tip", @"Tip");
            if ([invoiceData[@"tipPercent"] length] && [invoiceData[@"tipPercent"] integerValue] !=0)
            {
                tip = [NSString stringWithFormat:@"%@ (%@%%)", tip, invoiceData[@"tipPercent"]];
            }
            else
            {
                 tip = [NSString stringWithFormat:@"%@ (%@%%)", tip, invoiceData[@"tipPercent"]];
            }
            [Helper setToLabel:cell.itemLabel Text:tip WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *tipFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"tip"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:tipFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = NO;
            return cell;
        }
        else if (indexPath.row == 6)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Subtotal", @"Subtotal") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *subTotal =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"subTotal"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:subTotal WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else  if (indexPath.row == 8)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Credits Used", @"Credits Used") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"walletDeducted"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = NO;
            return cell;
        }
        else  if (indexPath.row == 9)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Total", @"Total") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"amount"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else  if (indexPath.row == 10)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Payment Type", @"Payment Type") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            [Helper setToLabel:cell.priceLabel Text:@"" WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = NO;
            return cell;
        }//"Wallet transaction
        else  if (indexPath.row == 11)
        {
            Receipt2TabelViewCell *cell = [cellArray lastObject];
            if ([invoiceData[@"payType"] integerValue] == 1)
            {
                [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Card", @"Card") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
                cell.cashCardImageView.image = [UIImage imageNamed:@"card"];
            }
            else
            {
                [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Cash", @"Cash") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
                cell.cashCardImageView.image = [UIImage imageNamed:@"cash"];
            }
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"cashCollected"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else  if (indexPath.row == 12)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Wallet Transaction", @"Wallet Transaction") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"walletTransaction"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
    }
    else
    {
        if (indexPath.row == 0)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Base Fare", @"Base Fare") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *baseFee = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"baseFee"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:baseFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else if (indexPath.row == 1)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Distance Fare",@"Distance Fare") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *distanceFee = [PatientGetLocalCurrency getCurrencyLocal:[flStrForStr(invoiceData[@"distanceFee"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:distanceFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else if (indexPath.row == 2)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Time Fare",@"Time Fare") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *timeFee = [PatientGetLocalCurrency getCurrencyLocal:[flStrForStr(invoiceData[@"timeFee"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:timeFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else if (indexPath.row == 3)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Airport Fare",@"Airport Fare") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *airportFee = [PatientGetLocalCurrency getCurrencyLocal:[flStrForStr(invoiceData[@"airportFee"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:airportFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else if (indexPath.row == 4)
        {
            ReceiptCell *cell = [cellArray firstObject];
            NSString *discountLabel = NSLocalizedString(@"Discount", @"Discount");
            if([invoiceData[@"code"] length])
            {
                if ([invoiceData[@"discountType"] integerValue] == 1)
                {
                    discountLabel = [NSString stringWithFormat:@"%@ ( %@ ) ( %@ %% )",discountLabel, flStrForStr(invoiceData[@"code"]), flStrForStr(invoiceData[@"discountVal"])];
                }
                else if ([invoiceData[@"discountType"] integerValue] == 2)
                {
                    NSString *discountWithCurrency = [PatientGetLocalCurrency getCurrencyLocal:[flStrForStr(invoiceData[@"discountVal"]) floatValue]];
                    discountLabel = [NSString stringWithFormat:@"%@ ( %@ ) ( %@ )", discountLabel, flStrForStr(invoiceData[@"code"]), discountWithCurrency];
                }
            }
            [Helper setToLabel:cell.itemLabel Text:discountLabel WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *discountFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"discount"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:discountFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = NO;
            return cell;
        }
        else if (indexPath.row == 5)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Subtotal",@"Subtotal") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *subTotal = [PatientGetLocalCurrency getCurrencyLocal:[flStrForStr(invoiceData[@"subTotal"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:subTotal WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
            
        }
        else if (indexPath.row == 6)
        {
            ReceiptCell *cell = [cellArray firstObject];
            NSString *tip = NSLocalizedString(@"Tip", @"Tip");
            if ([invoiceData[@"tipPercent"] length] && [invoiceData[@"tipPercent"] integerValue] !=0)
            {
                tip = [NSString stringWithFormat:@"%@ (%@%%)", tip, invoiceData[@"tipPercent"]];
            }
            else
            {
                tip = [NSString stringWithFormat:@"%@ (%@%%)", tip,invoiceData[@"tipPercent"]];
            }
            [Helper setToLabel:cell.itemLabel Text:tip WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *tipFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"tip"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:tipFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = NO;
            return cell;
        }
        else  if (indexPath.row == 7)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Credits Used", @"Credits Used") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"walletDeducted"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = NO;
            return cell;
        }

        else  if (indexPath.row == 8)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Total", @"Total") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"amount"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else  if (indexPath.row == 9)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Payment Type", @"Payment Type") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            [Helper setToLabel:cell.priceLabel Text:@"" WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = NO;
            return cell;
        }
        else  if (indexPath.row == 10)
        {
            Receipt2TabelViewCell *cell = [cellArray lastObject];
            if ([invoiceData[@"payType"] integerValue] == 1)
            {
                [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Card", @"Card") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
                cell.cashCardImageView.image = [UIImage imageNamed:@"card"];
            }
            else
            {
                [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Cash", @"Cash") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
                cell.cashCardImageView.image = [UIImage imageNamed:@"cash"];
            }
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"cashCollected"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else  if (indexPath.row == 11)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Wallet Transaction", @"Wallet Transaction") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"walletTransaction"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }

    }
    return nil;
}


- (NSString *)timeFormatted:(int)totalSeconds
{
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02dH:%02dM:%02dS",hours, minutes, seconds];
}


- (IBAction)closeBtnAction:(id)sender
{
    onCompletion(1);
    [self hidePOPup];
}

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window
{
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    invoiceData = [dict mutableCopy];
    [self.tableView reloadData];
    
    NSString *headingLabel = NSLocalizedString(@"RECEIPT", @"RECEIPT");
    NSString *bookingID = NSLocalizedString(@"BOOKING ID :", @"BOOKING ID :");
    bookingID = [NSString stringWithFormat:@"%@%@", bookingID, flStrForStr(invoiceData[@"bid"])];
    [self.closeBtn setTitle:NSLocalizedString(@"CLOSE",@"CLOSE") forState:UIControlStateNormal];
    
    
    NSMutableAttributedString *headingAttributedLabel = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@", headingLabel, bookingID]];
    
    [headingAttributedLabel addAttribute:NSForegroundColorAttributeName
                                   value:UIColorFromRGB(0x333333)
                                   range:NSMakeRange(0,[headingLabel length])];

    [headingAttributedLabel addAttribute:NSForegroundColorAttributeName
                                   value:UIColorFromRGB(0x6f6f6f)
                                   range:NSMakeRange([headingLabel length], [bookingID length]+1)];

    [headingAttributedLabel addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:Lato_Regular size:16.0]
                  range:NSMakeRange(0, [headingLabel length])];
    
    [headingAttributedLabel addAttribute:NSFontAttributeName
                                   value:[UIFont fontWithName:Lato_Regular size:12.0]
                                   range:NSMakeRange([headingLabel length], [bookingID length]+1)];

    [self.receiptLabel setAttributedText:headingAttributedLabel];
    
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;

    self.closeBtn.layer.cornerRadius = 4.0f;
    self.closeBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.closeBtn.layer.borderWidth = 1.0f;

    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)hidePOPup
{
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.contentView.alpha = 0.1;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}
@end
