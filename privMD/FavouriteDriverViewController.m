//
//  ListViewController.m
//  privMD
//
//  Created by Rahul Sharma on 19/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "FavouriteDriverViewController.h"
#import "FavouriteDriverViewCell.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"

enum doctorType
{
    Now = 1,
    Later = 2
};

@interface FavouriteDriverViewController ()<CustomNavigationBarDelegate>

@property(nonatomic,strong) UIButton *buttonNow;
@property(nonatomic,strong) UIButton *buttonLater;
@property(nonatomic,strong) NSMutableArray *favouriteDriverArray;
@property(nonatomic,assign) int *driverIndexToRemove;

@end


@implementation FavouriteDriverViewController
@synthesize listTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)customizeNavigationBackButton {
    
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(backButtonAction:)
              forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    
    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.listTableView.backgroundColor = [UIColor whiteColor];
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    self.listTableView.frame = CGRectMake(0, 64, screenSize.width, screenSize.height-64);
    self.title = NSLocalizedString(@"List", @"List");
    [self customizeNavigationBackButton];
    [self sendRequestToGetFavouritesDriver];
    _favouriteDriverArray = [NSMutableArray array];
    listTableView.allowsMultipleSelectionDuringEditing = NO;
    [self addCustomNavigationBar];
}

//-(void)viewDidAppear:(BOOL)animated
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Custom Methods -

- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"FAVOURITE DRIVER", @"FAVOURITE DRIVER")];
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    [self menuButtonPressedAccount];
}

- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


-(void)viewWillDisappear:(BOOL)animated {
    
    
}

-(void)goToRootController
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark UITableView DataSource -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [self favouriteDriverArray].count;
    if (count == 0) {
        count = 3;
    }
    return count;


}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return 1;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger count = [self favouriteDriverArray].count;
    if (count == 0) {
        return 170;
    }
    return 92;
    
}



-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger count = [self.favouriteDriverArray count];
    
    if (count == 0) {
        static NSString *CellIdentifier = @"noDataCell";
        UITableViewCell *cell = [[self listTableView] dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        [[cell textLabel] setTextAlignment:NSTextAlignmentCenter];
        [[cell textLabel] setTextColor:[UIColor colorWithWhite:0.2 alpha:0.8]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if ([indexPath section] == 1) {
            [[cell textLabel] setText:NSLocalizedString(@"You have not added any driver yet.", @"You have not added any driver yet.")];
        }
        else
        {
            [[cell textLabel] setText:@""];
        }
        return cell;
    }
    else {
        
        static NSString *cellIdentifier=@"Cell";
        FavouriteDriverViewCell *cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell==nil)
        {
            cell =[[FavouriteDriverViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];                                     
            cell.selectionStyle=UITableViewCellAccessoryNone;
            cell.backgroundColor=[UIColor clearColor];
        }
        
        NSDictionary *singleDriverDict = [_favouriteDriverArray objectAtIndex:indexPath.section];
        
        NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,singleDriverDict[@"image"]];
        
        [cell.docImage sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                         placeholderImage:[UIImage imageNamed:@"signup_image_user.png"]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                }];
        [cell.docName setText:[NSString stringWithFormat:@"%@ %@",singleDriverDict[@"name"],singleDriverDict[@"lname"]]];
        
        // Adding Star
        cell.starRatingView.value = [singleDriverDict[@"rating"]floatValue];
       
        [cell.docDistance setText:[NSString stringWithFormat:@"%@ %@",singleDriverDict[@"dis"],NSLocalizedString(@"miles away","miles away")]];
        return cell;
    }
}


// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        //add code here for when you hit delete
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message", @"Message") message:NSLocalizedString(@"Are you sure want to delete", @"Are you sure want to delete") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Delete", @"Delete"), nil];
        [alertView setTag:indexPath.section]; // Assigning here.
        [alertView show];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor colorWithRed:0.951 green:0.941 blue:0.941 alpha:1.000];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 20;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return 20;
    }
    return 0;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != [alertView cancelButtonIndex])
    {
        NSIndexPath * path = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
        
        NSDictionary *driverDict = [_favouriteDriverArray objectAtIndex:[path row]];
        NSString *driverEmail = driverDict[@"email"];
        
        [self sendRequestToRemoveDriverFromFavourites:driverEmail];
    }
    
}


-(void)sendRequestToGetFavouritesDriver {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..", @"Loading..")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *currentDate = [Helper getCurrentDateTime];
    latitude = [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLat]floatValue];
    longitude = [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLong]floatValue];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_date_time":currentDate,
                             @"ent_longitude":[NSNumber numberWithFloat:longitude],
                             @"ent_latitude":[NSNumber numberWithFloat:latitude],
                             };
    
    NSLog(@"Get Favourite Drivers Params = %@", params);
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getFavourites"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   NSLog(@"Get Favourite Drivers respose = %@", response);
                                   if (success)
                                   { //handle success response
                            //           TELogInfo(@"response %@",response);
                                       [self parseToGetFavouritesDriverResponse:response];
                                   }
                               }];
}

-(void)parseToGetFavouritesDriverResponse:(NSDictionary*)response {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (response == nil) {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            if (!_favouriteDriverArray) {
                
                _favouriteDriverArray = [[NSMutableArray alloc]init];
            }
            if([response[@"mastersOnline"] count])
                [_favouriteDriverArray addObjectsFromArray:response[@"mastersOnline"]];
            if([response[@"mastersOffline"] count])
                [_favouriteDriverArray addObjectsFromArray:response[@"mastersOffline"]];
            [listTableView reloadData];
        }
        else if ([[response objectForKey:@"errFlag"] integerValue] == 1) {
            
            // [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"errMsg"]];
        }
    }
}

-(void)sendRequestToRemoveDriverFromFavourites:(NSString *)driverEmail {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..", @"Loading..")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *currentDate = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_date_time":currentDate,
                             @"ent_mas_email":driverEmail,
                             };
    
    NSLog(@"Remove Favourite Drivers params = %@", params);
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"removeFavourites"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   NSLog(@"Remove Favourite Drivers response = %@", response);
                                   if (success)
                                   {
                                       //handle success response
                           //            TELogInfo(@"response %@",response);
                                       [self parseRemoveDriverFromFavouritesResponse:response];
                                   }
                               }];
}

-(void)parseRemoveDriverFromFavouritesResponse:(NSDictionary*)response {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (response == nil) {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            NSIndexPath *path = [listTableView indexPathForSelectedRow];
            
            [[self favouriteDriverArray]removeObjectAtIndex:path.row];
            
            [listTableView reloadData];
        }
    }
}

@end
