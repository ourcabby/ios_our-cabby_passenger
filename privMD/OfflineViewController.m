//
//  OfflineViewController.m
//  Tapp4
//
//  Created by Rahul Sharma on 05/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "OfflineViewController.h"
#import "OfflineTableViewCell.h"

@interface OfflineViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate, favDriverOfflineDelegate>
{
    BOOL inSearchMode;
    int pageIndex;
    int indexPage;
    NSString *keyWordForSearchBussiness;
    NSMutableArray *searchedData;
}
@property (nonatomic, strong) UILabel *messageLabel;

@end

@implementation OfflineViewController
@synthesize items;
@synthesize offlineTableView;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Display a message when the table is empty
    self.messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.messageLabel.text = NSLocalizedString(@"No offline drivers are currently available.", @"No offline drivers are currently available.");
    self.messageLabel.textColor = [UIColor blackColor];
    self.messageLabel.numberOfLines = 0;
    self.messageLabel.textAlignment = NSTextAlignmentCenter;
    self.messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
    self.messageLabel.tag = 1006;
    self.messageLabel.backgroundColor = [UIColor colorWithWhite:0.75 alpha:0.5];
    [self.messageLabel sizeToFit];
    offlineTableView.backgroundView = self.messageLabel;
    [self.messageLabel setHidden:YES];
    
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if (screenHeight == 480)
    {
        self.offlineTableView.frame = CGRectMake(0, 44, screenWidth, screenHeight-130);
    }
    else
    {
        self.offlineTableView.frame = CGRectMake(0, 44, screenWidth, screenHeight-44);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getDataFromParentClass2:(NSArray *)item
{
    items = [NSArray arrayWithArray: item];
    [offlineTableView reloadData];
}


#pragma mark - Table View Data source -
/*--------------------------------------------------------------------------------------------*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows;
    if(inSearchMode)
    {
        rows = searchedData.count;
    }
    else
    {
        if([items count])
        {
            rows = items.count;
            [self.messageLabel setHidden:YES];
        }
        else
        {
            rows = 0;
            [self.messageLabel setHidden:NO];
        }
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"offlineCell";
    OfflineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(inSearchMode)
    {
        [cell loadLabelData:[searchedData objectAtIndex:indexPath.row] index:indexPath];
    }
    else if(items.count)
    {
        [cell loadLabelData:[items objectAtIndex:indexPath.row] index:indexPath];
    }
    cell.delegate = self;
    [cell bringSubviewToFront:cell.backgroundView];
    return cell;
}

#pragma mark - TableView delegate
/*--------------------------------------------------------------------------------------------*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark Delegate method

- (void)addFavouriteBtnDelegate:(NSDictionary *)data
{
    NSMutableDictionary *notificationData = [[NSMutableDictionary alloc] initWithDictionary:data];
    int index = [data[@"index"] integerValue]-1;
    if (inSearchMode)
    {
        [notificationData setObject:searchedData[index][@"email"] forKey:@"driverEmail"];
    }
    else
    {
        [notificationData setObject:items[index][@"email"] forKey:@"driverEmail"];
    }
    inSearchMode = NO;
    [self searchBarCancelButtonClicked:self.offlineSearchBar];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateFavouriteDriver" object:nil userInfo:notificationData];
}



#pragma mark - SearchBar Delegates
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    searchedData = [NSMutableArray array];
    [self.offlineTableView reloadData];
    keyWordForSearchBussiness = searchBar.text;
    [self filterContent:searchBar.text];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = nil;
    keyWordForSearchBussiness = nil;
    [searchBar resignFirstResponder];
    [searchedData removeAllObjects];
    inSearchMode = NO;
    [self.offlineTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(inSearchMode)
    {
        searchedData = [[NSMutableArray alloc] init];
    }
    keyWordForSearchBussiness = searchText;
    [self filterContent:searchText];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if ([self.offlineTableView isUserInteractionEnabled])
    {
        inSearchMode = true;
        [searchBar setShowsCancelButton:inSearchMode animated:true];
        [self.offlineTableView reloadData];
    }
    else
    {
        [self.view endEditing:YES];
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    NSString *searchText = searchBar.text;
    // We're still 'in edit mode', if the user left a keyword in the searchBar
    inSearchMode = (searchText != nil && [searchText length] > 0);
    [searchBar setShowsCancelButton:inSearchMode animated:true];
    [self filterContent:searchText];
}

- (void)filterContent:(NSString*)searchText
{
    if(inSearchMode)
    {
        // Populate the results
        NSString *searchString = searchText;
        NSArray *filtered = [items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(name CONTAINS[c] %@)", searchString]];
        searchedData =[filtered mutableCopy];
        [self.offlineTableView reloadData];
    }
}


@end
