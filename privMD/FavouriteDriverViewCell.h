//
//  ListViewCell.h
//  privMD
//
//  Created by Rahul Sharma on 19/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView/AXRatingView.h>
#import "RoundedImageView.h"
@class CSAnimationView;
@interface FavouriteDriverViewCell : UITableViewCell
//@property (strong,nonatomic) UIImageView *docImage;
@property (strong,nonatomic) UILabel *docName;
@property (nonatomic,strong) AXRatingView *starRatingView;
@property (nonatomic,strong) RoundedImageView *docImage;
@property (nonatomic,strong) CSAnimationView *animationView;
@property (strong,nonatomic) UILabel *docDistance;
@property(strong, nonatomic)  UIActivityIndicatorView *activityIndicator;

@end
