//
//  WalletViewController.m
//  RoadyoDispatch
//
//  Created by 3Embed on 20/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "WalletViewController.h"
#import "WalletHistoryViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "PatientViewController.h"
#import "PatientGetLocalCurrency.h"
#import "PaymentViewController.h"

@interface WalletViewController ()<CustomNavigationBarDelegate, XDKAirMenuDelegate>
{
    float initialOffset_Y;
    float keyboardHeight;
    NSString *currencyStr;
}

@property (strong,nonatomic)  UITextField *activeTextField;
@end

@implementation WalletViewController
@synthesize mainScrollView;
@synthesize activeTextField;
@synthesize addMoneyTextField, promoCodeTextField;
@synthesize promoCodeCheckMarkBtn;
@synthesize containerView, balanceView, balanceAddView;
@synthesize addMoneyBtn1, addMoneyBtn2, addMoneyBtn3;

- (void)viewDidLoad
{
    [super viewDidLoad];
    currencyStr =[PatientGetLocalCurrency getCurrencyLocalForTip:[@"" floatValue]];//NSLocalizedString(@"EGP ", @"EGP ");
    [self addCustomNavigationBar];
    [self updateUI];
    [self getWalletCurrentMoney];
}

-(void)updateUI {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    balanceView.frame = CGRectMake(0, 0, screenWidth-20, 80);
    balanceAddView.frame = CGRectMake(0, 80, screenWidth-20, 200);
    containerView.frame = CGRectMake(10, 150, screenWidth-20, 300);
    containerView.layer.borderWidth = 2.0f;
    containerView.layer.borderColor = UIColorFromRGB(0xe1e1e1).CGColor;
    
    [Helper setToLabel:self.walletLabel1 Text:NSLocalizedString(@"Do more with Taxima Money.",@"Do more with Taxima Money") WithFont:Lato FSize:14 Color:UIColorFromRGB(0x23982B)];
    [Helper setToLabel:self.walletLabel2 Text:NSLocalizedString(@"One tap access to all your daily needs",@"One tap access to all your daily needs") WithFont:Lato FSize:14 Color:UIColorFromRGB(0x23982B)];
    [Helper setToLabel:self.addMoneyLabel Text:NSLocalizedString(@"Add Taxima Money",@"Add Taxima Money") WithFont:Lato FSize:14 Color:UIColorFromRGB(0x666666)];
    [Helper setToLabel:self.quickSafeDescriptionLabel Text:NSLocalizedString(@"It's quick, safe and secure", @"It's quick, safe and secure") WithFont:Lato FSize:11    Color:UIColorFromRGB(0x666666)];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    mainScrollView.backgroundColor = [UIColor clearColor];
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    mainScrollView.contentSize = CGSizeMake(screenSize.size.width, screenSize.size.height);
}

-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


#pragma mark- Custom Methods

- (void) addCustomNavigationBar
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"MY WALLET", @"MY WALLET")];
    [customNavigationBarView hideRightBarButton:NO];
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    [self menuButtonclicked];
}

- (IBAction)historyBtnAction:(id)sender
{
    [self.view endEditing:YES];
    [self performSegueWithIdentifier:@"walletHistoryVC" sender:self];
}


- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

#pragma mark - TextFields

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    activeTextField = textField;
    textField.text = currencyStr;
    [addMoneyBtn1 setSelected: NO];
    [addMoneyBtn2 setSelected: NO];
    [addMoneyBtn3 setSelected: NO];
    [self moveViewUp:textField andKeyboardHeight:keyboardHeight];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [textField becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField.text length] > 0) {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ([reachability isNetworkAvailable]) {
            if ([textField isEqual:addMoneyTextField]) {
            } else if ([textField isEqual:promoCodeTextField]) {
            }
        } else {
            [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"No Network", @"No Network") On:self.view];
        }
    }
    self.activeTextField = nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *typedText = textField.text;
    typedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (typedText.length <= currencyStr.length) {
        textField.text = currencyStr;
        return NO;
    }
    if (typedText.length) {
        NSString *amount;
        NSRange replaceRange = [typedText rangeOfString:currencyStr];
        if (replaceRange.location != NSNotFound){
            amount = [typedText stringByReplacingCharactersInRange:replaceRange withString:@""];
        }
        switch ([amount integerValue]) {
            case 499:
                [addMoneyBtn1 setSelected:YES];
                break;
            case 999:
                [addMoneyBtn2 setSelected:YES];
                break;
            case 1999:
                [addMoneyBtn3 setSelected:YES];
                break;
            default:
                [addMoneyBtn1 setSelected: NO];
                [addMoneyBtn2 setSelected: NO];
                [addMoneyBtn3 setSelected: NO];
                break;
        }
    }
    return YES;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/*--------------------------------------------*/
#pragma -marks Keyboard Appear and Hide Methods
/*--------------------------------------------*/
/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:keyboardHeight];
}

/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification {
    [self moveViewDown];
}
/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height {
    float textfieldMaxY = CGRectGetMinY([[[textfield superview] superview] superview].frame);
    textfieldMaxY = textfieldMaxY + CGRectGetMinY([[textfield superview] superview].frame);
    textfieldMaxY = textfieldMaxY + CGRectGetMinY([textfield superview].frame);
    textfieldMaxY = textfieldMaxY + CGRectGetMaxY(textfield.frame);
    float remainder = CGRectGetHeight(self.view.frame) - (textfieldMaxY + height) + initialOffset_Y;
    if (remainder >= 0) {
        return;
    }
    [UIView animateWithDuration:0.4
                     animations:^{
                         mainScrollView.contentOffset = CGPointMake(0, initialOffset_Y - remainder);
                     }];
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown {
    mainScrollView.contentOffset = CGPointMake(0, initialOffset_Y);
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"walletHistoryVC"]) {
    }
}


- (IBAction)addMoneyBtn1Action:(id)sender {
    NSString *str = NSLocalizedString(@"499", @"499");
    addMoneyTextField.text = [NSString stringWithFormat:@"%@%@", currencyStr, str];
    [addMoneyBtn1 setSelected: YES];
    [addMoneyBtn2 setSelected: NO];
    [addMoneyBtn3 setSelected: NO];

}

- (IBAction)addMoneyBtn2Action:(id)sender {
    NSString *str = NSLocalizedString(@"999", @"999");
    addMoneyTextField.text = [NSString stringWithFormat:@"%@%@", currencyStr, str];
    [addMoneyBtn1 setSelected: NO];
    [addMoneyBtn2 setSelected: YES];
    [addMoneyBtn3 setSelected: NO];
}

- (IBAction)addMoneyBtn3Action:(id)sender {
    NSString *str = NSLocalizedString(@"1999", @"1999");
    addMoneyTextField.text = [NSString stringWithFormat:@"%@%@", currencyStr, str];
    [addMoneyBtn1 setSelected: NO];
    [addMoneyBtn2 setSelected: NO];
    [addMoneyBtn3 setSelected: YES];
}

- (IBAction)promoCodeApplyBtnAction:(id)sender {
}

- (IBAction)addMoneyBtnAction:(id)sender {
    NSString *textFiledText = flStrForObj(addMoneyTextField.text);
    NSString *amount = @"";
    NSRange replaceRange = [textFiledText rangeOfString:currencyStr];
    if (replaceRange.location != NSNotFound){
        amount = [textFiledText stringByReplacingCharactersInRange:replaceRange withString:@""];
    }
    NSInteger number = [amount integerValue];
    if (number == 0) {
        [Helper showAlertWithTitle:@"Message" Message:@"First enter amount"];
        return;
    }else if(number < 100) {
        [Helper showAlertWithTitle:@"Message" Message:[NSString stringWithFormat:@"%@%@%@",@"The amount need to be more than",currencyStr,@"99"]];//@"The amount need to be more than EGP 99"];
        return;
    } else {
        [self goToPaymentController:(NSString *)amount];
    }
}

- (IBAction)promoCodeCheckMarkBtnAction:(id)sender {
    if (promoCodeCheckMarkBtn.isSelected){
        [UIView animateWithDuration:0.9f animations:^{
            [self. promoCodeView setHidden:YES];
        }];
        promoCodeCheckMarkBtn.selected = NO;
    } else {
        [UIView animateWithDuration:0.5f animations:^{
            [self. promoCodeView setHidden:NO];
        }];
        promoCodeCheckMarkBtn.selected = YES;
    }
}


-(void)goToPaymentController:(NSString *)amount {
//    PaymentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"paymentView"];
//    vc.callback = ^(NSString *cardIde, NSString *type, NSString *last4){
//        [self addMoney:(NSString *)amount ToWalletWithCardID:cardIde];
//    };
//    vc.isComingFromSummary = YES;
//    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:vc];
//    [self presentViewController:navBar animated:YES completion:nil];
}


#pragma mark - WebServices -

-(void) getWalletCurrentMoney {
    NSString *sid = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUSID]);
    NSDictionary *params = @{
                             @"ent_sid":flStrForStr(sid),
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"GetWalletMoney"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self getWalletCurrentMoneyResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Checking Current Balance...", @"Checking Current Balance...")];
}

-(void) getWalletCurrentMoneyResponse:(NSDictionary *) responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil) {
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
          [Helper removeToken];
        [XDKAirMenuController relese];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDict[@"errMsg"]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    } else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        NSString *currentBal = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(responseDict[@"amount"]) floatValue]];
        self.currentBalanceLabel.text = [NSString stringWithFormat:@"%@",currentBal];
        if ([responseDict[@"amount"] integerValue] <= 0) {
            [Helper setToLabel:self.balanceStatusLabel Text:NSLocalizedString(@"Balance is low", @"Balance is low") WithFont:Lato FSize:13 Color:UIColorFromRGB(0xeb1313)];
        } else {
            [Helper setToLabel:self.balanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:Lato FSize:13 Color:UIColorFromRGB(0x666666)];
        }
    }
}

-(void) addMoney:(NSString *)amount ToWalletWithCardID:(NSString *)carID {
    NSUserDefaults *udPlotting = [NSUserDefaults standardUserDefaults];
    NSString *sessionToken = [udPlotting objectForKey:KDAcheckUserSessionToken];
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
        deviceID = kPMDTestDeviceidKey;
    else
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_amount":flStrForStr(amount),
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"AddMoneyToWallet"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                       [self addMoneyToWalletResponse:response];
                               }];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Adding Money...", @"Adding Money...")];
}

-(void) addMoneyToWalletResponse:(NSDictionary *) responseDict {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil){
        return;
    } else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 )) {
        [Helper removeToken];
        [XDKAirMenuController relese];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDict[@"errMsg"]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    } else if ([[responseDict objectForKey:@"errFlag"] intValue] == 1) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
    } else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:flStrForStr(responseDict[@"errMsg"])];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        NSString *currentBal = [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(responseDict[@"amount"]) floatValue]];
        self.addMoneyTextField.text = @"";
        self.currentBalanceLabel.text = [NSString stringWithFormat:@"%@",currentBal];
        if ([responseDict[@"amount"] integerValue] <= 0) {
            [Helper setToLabel:self.balanceStatusLabel Text:NSLocalizedString(@"Balance is low",@"Balance is low") WithFont:Lato FSize:13 Color:UIColorFromRGB(0xeb1313)];
        } else {
            [Helper setToLabel:self.balanceStatusLabel Text:NSLocalizedString(@"Current Balance",@"Current Balance") WithFont:Lato FSize:13 Color:UIColorFromRGB(0x666666)];
        }
    }
}

@end
