//
//  PastOrderTableViewCell.h
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 12/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView/AXRatingView.h>

@protocol favDriverOnlineDelegate <NSObject>
@optional
-(void) addFavouriteBtnDelegate:(NSDictionary *)delegateData;
@end

@interface OnineTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *carDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIView *ratingview;
@property (strong, nonatomic) UIActivityIndicatorView * activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *addFavouriteBtn;
@property(nonatomic,strong)AXRatingView *ratingViewStars;
@property (nonatomic,assign) id<favDriverOnlineDelegate> delegate;

- (IBAction)addFavouriteBtn:(id)sender;

-(void)loadLabelData:(NSDictionary *) drvireDetails index:(NSIndexPath *)indexPath;

@end
