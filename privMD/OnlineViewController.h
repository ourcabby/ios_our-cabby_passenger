//
//  PastOrderViewController.h
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 12/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *onlineTableView;
@property (strong, nonatomic) NSArray *items;
@property (weak, nonatomic) IBOutlet UISearchBar *onlineSearchBar;

-(void)getDataFromParentClass1:(NSArray *)item;
@end
