//
//  Fav1ViewController.h
//  Tapp4
//
//  Created by Rahul Sharma on 05/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Fav1ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (weak, nonatomic) IBOutlet UIButton *offlineBtn;
@property (weak, nonatomic) IBOutlet UIButton *onlineBtn;
@property (weak, nonatomic) IBOutlet UIButton *favouriteBtn;

- (IBAction)onlineBtnClick:(id)sender;
- (IBAction)offlineBtnClick:(id)sender;
- (IBAction)favDriverBtnClick:(id)sender;

@end
