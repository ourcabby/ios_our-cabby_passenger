//
//  OfflineViewController.h
//  Tapp4
//
//  Created by Rahul Sharma on 05/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfflineViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *offlineTableView;
@property (strong, nonatomic) NSArray *items;
@property (weak, nonatomic) IBOutlet UISearchBar *offlineSearchBar;

-(void)getDataFromParentClass2:(NSArray *)items;

@end
