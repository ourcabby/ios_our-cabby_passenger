//
//  CurrentOrderTableViewCell.m
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 12/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "Favourite1TableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation Favourite1TableViewCell
@synthesize activityIndicator;
@synthesize ratingview,ratingViewStars;
@synthesize driverImageView, driverNameLabel, carDetailsLabel, distanceLabel, addFavouriteBtn;
@synthesize delegate;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (IBAction)addFavouriteBtn:(id)sender
{
    NSString * addOrRemove;
    if (!addFavouriteBtn.selected)
    {
        addFavouriteBtn.selected = YES;
        addOrRemove = @"1";
    }
    else
    {
        addFavouriteBtn.selected  = NO;
        addOrRemove = @"2";
    }
    if (delegate && [delegate respondsToSelector:@selector(addFavouriteBtnDelegate:)])
    {
        NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys: [NSNumber numberWithInt:addFavouriteBtn.tag], @"index", @"2", @"isComingFrom", addOrRemove, @"addOrRemove", nil];
        [delegate addFavouriteBtnDelegate:data];
    }
}

-(void)loadLabelData:(NSDictionary *) drvireDetails index:(NSIndexPath *)indexPath
{
    CGRect fra = driverImageView.frame;
    fra.origin.x = driverImageView.frame.size.width/2-10;
    fra.origin.y = driverImageView.frame.size.height/2-10;
    fra.size.width = 20;
    fra.size.height = 20;
    activityIndicator = [[UIActivityIndicatorView alloc]init];
    activityIndicator.frame = fra;
    [driverImageView addSubview:activityIndicator];
    activityIndicator.color = [UIColor blueColor];
    activityIndicator.backgroundColor=[UIColor clearColor];
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityIndicator startAnimating];
    
    self.driverNameLabel.text = flStrForStr([drvireDetails objectForKey:@"name"]);
    self.carDetailsLabel.text = flStrForStr([drvireDetails objectForKey:@"vechicleDesc"]);
    self.distanceLabel.text = [NSString stringWithFormat:@"%@ %@ away",[NSString stringWithFormat:@"%.02f",[[drvireDetails objectForKey:@"dis"] floatValue]/kPMDDistanceMetric], kPMDDistanceParameter];

    addFavouriteBtn.tag = indexPath.row + 1;    
    if ([drvireDetails[@"fav"] integerValue] == 1)
    {
        addFavouriteBtn.selected = YES;
    }
    else
    {
        addFavouriteBtn.selected = NO;
    }
    
    driverImageView.layer.borderColor = [UIColor blackColor].CGColor;
    driverImageView.layer.borderWidth = 1.0f;
    driverImageView.layer.masksToBounds = NO;
    driverImageView.layer.cornerRadius = driverImageView.frame.size.width/2;
    driverImageView.clipsToBounds = YES;
    
    ratingViewStars = [[AXRatingView alloc] initWithFrame:CGRectMake(0,0,230,23)];
    ratingViewStars.stepInterval = 0.0;
    ratingViewStars.markFont = [UIFont systemFontOfSize:20.0f];
    ratingViewStars.baseColor = [UIColor colorWithRed:0.293 green:0.303 blue:0.322 alpha:1.000];
    ratingViewStars.highlightColor = [UIColor colorWithRed:0.975 green:0.736 blue:0.056 alpha:1.000];
    ratingViewStars.value = [[drvireDetails objectForKey:@"rating"] floatValue];
    ratingViewStars.userInteractionEnabled = NO;
    [ratingview addSubview:ratingViewStars];
    
    
    NSString *imageURL = [NSString stringWithFormat:@"%@%@", baseUrlForOriginalImage,flStrForStr([drvireDetails objectForKey:@"image"])] ;
    if([imageURL length])
    {
        [driverImageView sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"signup_image_user.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (!error)
             {
                 driverImageView.image = [self imageWithImage:image scaledToSize:CGSizeMake(50, 50)];
                 [activityIndicator stopAnimating];
             }
             else
             {
                 [activityIndicator stopAnimating];
             }
         }];
    }
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


@end
