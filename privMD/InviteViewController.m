//
//  InviteViewController.m
//  UBER
//
//  Created by Rahul Sharma on 06/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "InviteViewController.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
//#import <FacebookSDK/FacebookSDK.h>
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "TeleGramHandeler.h"
//#import "ImogramHandler.h"
#import <Facebook-iOS-SDK/FBSDKShareKit/FBSDKShareKit.h>

typedef enum{
    kSendText = 0,
    kSendImage,
    kSendTextWithImage,
    kSendAudio,
    kSendCancel
}options;

@interface InviteViewController ()<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,CustomNavigationBarDelegate,FBSDKSharingDelegate>
{
    MFMailComposeViewController *mailer;
    NSString *shareText;
    UIDocumentInteractionController *docControll;
    UIImageView *imageView;
}
@property (strong, nonatomic) IBOutlet UILabel *shareLable;
@property (strong, nonatomic) IBOutlet UILabel *promoCodeMsgShowingLabel;
@property (strong, nonatomic) IBOutlet UILabel *promoCodeLablel;
@property (strong, nonatomic) IBOutlet UIButton *fbButton;
@property (strong, nonatomic) IBOutlet UILabel *fbShareLabel;
@property (strong, nonatomic) IBOutlet UIButton *twitterButton;
@property (strong, nonatomic) IBOutlet UILabel *twitterShareLAble;
@property (strong, nonatomic) IBOutlet UIButton *smsButton;
@property (strong, nonatomic) IBOutlet UILabel *smsShareLabel;
@property (strong, nonatomic) IBOutlet UIButton *emailButton;
@property (strong, nonatomic) IBOutlet UILabel *emailShareLable;
@property (strong, nonatomic) IBOutlet UILabel *shareyourcodeLabel;

- (IBAction)facebookButtonClicked:(id)sender;
- (IBAction)twitterButtonClicked:(id)sender;
- (IBAction)smsButtonClicked:(id)sender;
- (IBAction)emailButtonClicked:(id)sender;

@end

@implementation InviteViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addCustomNavigationBar];
    NSString *pCode = flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:kNSUUserReferralCode]);
    [Helper setToLabel:_shareLable Text:NSLocalizedString(@"SHARE OUR CABBY", @"SHARE OUR CABBY") WithFont:Roboto_Bold FSize:12 Color:UIColorFromRGB(0x656565)];
    shareText = NSLocalizedString(@"Use my OUR CABBY code, ", @"Use my OUR CABBY code, ");
    NSString *str2 = NSLocalizedString(@"and get discount on your first OUR CABBY booking.", @"and get discount on your first OUR CABBY booking.");
    NSString *str3 = NSLocalizedString(@"To download go to", @"To download go to");
    NSString *link = [NSString stringWithFormat:@"https://%@", itunesURL];
    shareText = [shareText stringByAppendingString:pCode];
    shareText = [NSString stringWithFormat:@"%@ , %@ %@ %@", shareText,str2,str3,link];
    
    NSString *str = @"";
    str = @"Share your referral code. On every signup from your referral, earn an exclusive promo code!";
    
    [Helper setToLabel:_promoCodeMsgShowingLabel Text:str WithFont:Roboto_Light FSize:14 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_promoCodeLablel Text:pCode WithFont:Roboto_Light FSize:33 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_shareyourcodeLabel Text:NSLocalizedString(@"SHARE YOUR CODE", @"SHARE YOUR CODE") WithFont:Roboto_Light FSize:12 Color:UIColorFromRGB(0x333333)];

    [Helper setToLabel:_fbShareLabel Text:NSLocalizedString(@"FACEBOOK", @"FACEBOOK") WithFont:Lato_Bold FSize:10 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_twitterShareLAble Text:NSLocalizedString(@"TWITTER", @"TWITTER") WithFont:Lato_Bold FSize:10 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_smsShareLabel Text:@"MESSAGE" WithFont:Lato_Bold FSize:10 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_emailShareLable Text:NSLocalizedString(@"EMAIL", @"EMAIL") WithFont:Lato_Bold FSize:10 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:self.whatsLabel Text:NSLocalizedString(@"WHATSAPP", @"WHATSAPP") WithFont:Lato_Bold FSize:10 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:self.telegramLabel Text:NSLocalizedString(@"TELEGRAM", @"TELEGRAM") WithFont:Lato_Bold FSize:10 Color:UIColorFromRGB(0x333333)];
    
}

#pragma mark- Custom Methods

- (void) addCustomNavigationBar
{
    
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"SHARE", @"SHARE")];
    [self.view addSubview:customNavigationBarView];
    
}
-(void)leftBarButtonClicked:(UIButton *)sender{

    [self menuButtonclicked];
}

- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)facebookButtonClicked:(id)sender
{
    [self shareOnFacebook];
}

- (IBAction)twitterButtonClicked:(id)sender
{
    [self shareOnTwitter];
}

- (IBAction)smsButtonClicked:(id)sender
{
    [self shareViaSMS];
}

- (IBAction)emailButtonClicked:(id)sender
{
    [self EmaiSharing];
}

-(void)shareViaSMS
{    
    if(![MFMessageComposeViewController canSendText])
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT", @"ALERT") message:NSLocalizedString(@"Your device doesn't support SMS!", @"Your device doesn't support SMS!") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *message = [NSString stringWithFormat:@"%@",shareText];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [[mailer navigationBar] setTintColor: [UIColor blackColor]];
    mailer.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];

    //[messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Failed to send !", @"Failed to send !") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
        [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)shareOnTwitter
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
           NSString *message = [NSString stringWithFormat:@"%@",shareText];
        
        [tweetSheet setInitialText:message];
        
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgLinkForSharing]]];
        [tweetSheet addImage:image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"Message", @"Message")
                                  message:NSLocalizedString(@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup", @"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup")
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                  otherButtonTitles:nil];
        [alertView show];
    }
    
}

-(void)shareOnFacebook
{
    [self publishWithWebDialog];
}

/*
 * Share using the Web Dialog
 */

- (void)publishWithWebDialog
{
    // Put together the dialog parameters
    FBSDKShareLinkContent *condense =[[FBSDKShareLinkContent alloc] init];
    condense.contentURL = [NSURL URLWithString:itunesURL];
    [condense setImageURL:[NSURL URLWithString:imgLinkForSharing]];
  //  condense.imageURL = [NSURL URLWithString:imgLinkForSharing];
    condense.contentTitle= [NSString stringWithFormat: @"%@",NSLocalizedString(@"OUR CABBY", @"OUR CABBY")];
    condense.contentDescription=[NSString stringWithFormat:@"%@",shareText];
    [FBSDKShareDialog showFromViewController:self withContent:condense delegate:self];

//    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
//    content.contentURL = [NSURL URLWithString:itunesURL];
//    content.imageURL=[NSURL URLWithString:imgLinkForSharing];
//    
//    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
//    dialog.shareContent = content;
//    dialog.fromViewController = self;
//    dialog.mode = FBSDKShareDialogModeFeedBrowser;
//    [dialog show];
    
    
    
    
      //  NSString *message = [NSString stringWithFormat:@"%@",shareText];
    
  //  NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"OUR CABBY", @"OUR CABBY"),@"name",
                         //          message,@"description",imgLinkForSharing,@"picture",nil];
    
    
 //   [FBWebDialogs presentFeedDialogModallyWithSession:nil
     //                                      parameters:params
     //                                         handler:
   //  ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
    //     if (error) {
             // Error launching the dialog or publishing a story.
    //         [self showAlert:[self checkErrorMessage:error]];
    //     } else {
       //      if (result == FBWebDialogResultDialogNotCompleted) {
     //        } else {
                 // Handle the publish feed callback
     //            NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
      //           if (![urlParams valueForKey:@"post_id"]) {

        //         } else {
       //              // User clicked the Share button
        //             [self showAlert:[self checkPostId:urlParams]];
      //           }
      //       }
     //    }
    // }];
}

/*
 * Helper method to parse URL parameters.
 */
- (NSDictionary*)parseURLParams:(NSString *)query
{
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [[kv objectAtIndex:1]
         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}


#pragma mark - Helper methods

- (NSString *)checkErrorMessage:(NSError *)error
{
    NSString *errorMessage = @"";
    errorMessage = NSLocalizedString(@"Operation failed due to a connection problem, retry later.", @"Operation failed due to a connection problem, retry later.");
    return errorMessage;
}

- (NSString *) checkPostId:(NSDictionary *)results {
   
    NSString *message = NSLocalizedString(@"Posted successfully.", @"Posted successfully.");
   
    return message;
}

- (void)showAlert:(NSString *) alertMsg {
    if (![alertMsg isEqualToString:@""]) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Result", @"Result")
                                    message:alertMsg
                                   delegate:nil
                          cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                          otherButtonTitles:nil] show];
    }
}

-(void)EmaiSharing
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:Roboto_Bold size:14], NSFontAttributeName, [UIColor blackColor], NSForegroundColorAttributeName, nil];
        
        [[UINavigationBar appearance] setTitleTextAttributes:attributes];
        mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [[mailer navigationBar] setTintColor: [UIColor blackColor]];
        mailer.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];

     
        NSString *shareUrl =  [NSString stringWithFormat:@"%@",shareText];
        [mailer setSubject:NSLocalizedString(@"OUR CABBY", @"OUR CABBY")];
        NSArray *toRecipents;
        NSMutableString* message =[[NSMutableString alloc] init];
        [message appendString:shareUrl];
        toRecipents = [NSArray arrayWithObject:@""];
        [mailer setMessageBody:message isHTML:NO];
        [mailer setToRecipients:toRecipents];
        NSString *strURL = [NSString stringWithFormat:@"%@",imgLinkForSharing];
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]]];
        
        NSData *photoData = UIImageJPEGRepresentation(image,1);
        [mailer addAttachmentData:photoData mimeType:@"image/jpg" fileName:[NSString stringWithFormat:@"photo.png"]];
        
        [self presentViewController:mailer animated:YES completion:^{
            [[UINavigationBar appearance] setTitleTextAttributes:attributes];
        }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Failure", @"Failure")
                                                        message:NSLocalizedString(@"Your device doesn't support the composer sheet", @"Your device doesn't support the composer sheet")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - MFMailComposeViewController Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)whatsupAction:(id)sender
{
    UIAlertController *as = [UIAlertController alertControllerWithTitle:@"Choose"
                                                                message:nil
                                                         preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Text
    [as addAction:[UIAlertAction actionWithTitle:@"Send text"
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action) {
                                            [[WASWhatsAppUtil getInstance] sendText:shareText];
                                         }]];
    
    // Image
    [as addAction:[UIAlertAction actionWithTitle:@"Send image"
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action) {
                                             UIImage *image=[[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgLinkForSharing]]];
                                             NSData *data = UIImageJPEGRepresentation(image, 1.0);
                                             [[WASWhatsAppUtil getInstance] sendFile:data UTI:WhatsAppImageType inView:self.view];
                                             
                                         }]];
    
    
    // Cancel
    [as addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                           style:UIAlertActionStyleCancel
                                         handler:^(UIAlertAction *action) {
                                             
                                             [self dismissViewControllerAnimated:YES completion:^{}];
                                         }]];
    
    [self presentViewController:as animated:YES completion:nil];
}
- (IBAction)telegramAction:(id)sender
{
    UIAlertController *as = [UIAlertController alertControllerWithTitle:@"Choose"
                                                                message:nil
                                                         preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Text
    [as addAction:[UIAlertAction actionWithTitle:@"Send text"
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action) {
                                             [[TeleGramHandeler getInstance] sendText1:shareText];
                                         }]];
    
    // Image
    [as addAction:[UIAlertAction actionWithTitle:@"Send image"
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action) {
                                             UIImage *image=[[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgLinkForSharing]]];
                                             NSData *data = UIImageJPEGRepresentation(image, 1.0);
                                             [[TeleGramHandeler getInstance] sendFile1:data UTI:TeleGramImageType inView:self.view];
                                         }]];
    
    // Cancel
    [as addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                           style:UIAlertActionStyleCancel
                                         handler:^(UIAlertAction *action) {
                                             
                                             [self dismissViewControllerAnimated:YES completion:^{}];
                                         }]];
    
    [self presentViewController:as animated:YES completion:nil];
}


@end
