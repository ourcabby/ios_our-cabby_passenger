//
//  ListViewController.h
//  privMD
//
//  Created by Rahul Sharma on 19/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FavouriteDriverViewController : UIViewController<UIActionSheetDelegate>
{
    float latitude;
    float longitude;
}
@property (strong, nonatomic) IBOutlet UITableView *listTableView;


@end
