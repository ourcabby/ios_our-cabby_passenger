//
//  PastOrderViewController.m
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 12/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "OnlineViewController.h"
#import "Fav1ViewController.h"
#import "OnineTableViewCell.h"

@interface OnlineViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate, favDriverOnlineDelegate>
{
    BOOL inSearchMode;
    int pageIndex;
    int indexPage;
    NSString *keyWordForSearchBussiness;
    NSMutableArray *searchedData;
}
@property (nonatomic, strong) UILabel *messageLabel;

@end

@implementation OnlineViewController
@synthesize items;
@synthesize onlineTableView;

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.messageLabel.text = NSLocalizedString(@"No onlie drivers are currently available.", @"No online drivers are currently available.");
    self.messageLabel.textColor = [UIColor blackColor];
    self.messageLabel.numberOfLines = 0;
    self.messageLabel.textAlignment = NSTextAlignmentCenter;
    self.messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
    self.messageLabel.tag = 1007;
    self.messageLabel.backgroundColor = [UIColor colorWithWhite:0.75 alpha:0.5];
    [self.messageLabel sizeToFit];
    onlineTableView.backgroundView = self.messageLabel;
    onlineTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.messageLabel setHidden:YES];
    
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if (screenHeight == 480)
    {
        self.onlineTableView.frame = CGRectMake(0, 44, screenWidth, screenHeight-130);
    }
    else
    {
        self.onlineTableView.frame = CGRectMake(0, 44, screenWidth, screenHeight-44);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



-(void)getDataFromParentClass1:(NSArray *)item
{
    items = [[NSArray alloc] initWithArray:item];
    [onlineTableView reloadData];
}

#pragma mark - Table View Data source -
/*--------------------------------------------------------------------------------------------*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows;
    if(inSearchMode)
    {
        rows = searchedData.count;
    }
    else
    {
        if([items count])
        {
            rows = items.count;
            [self.messageLabel setHidden:YES];
        }
        else
        {
            rows = 0;
            [self.messageLabel setHidden:NO];
        }
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"onlineCell";
    OnineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(inSearchMode)
    {
        [cell loadLabelData:[searchedData objectAtIndex:indexPath.row] index:indexPath];
    }
    else if(items.count)
    {
        [cell loadLabelData:[items objectAtIndex:indexPath.row] index:indexPath];
    }
    cell.delegate = self;
    [cell bringSubviewToFront:cell.backgroundView];
    return cell;
}


#pragma mark - TableView delegate
/*--------------------------------------------------------------------------------------------*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark Delegate method

- (void)addFavouriteBtnDelegate:(NSDictionary *)data
{
    NSMutableDictionary *notificationData = [[NSMutableDictionary alloc] initWithDictionary:data];
    int index = [data[@"index"] integerValue]-1;
    if (inSearchMode)
    {
        [notificationData setObject:searchedData[index][@"email"] forKey:@"driverEmail"];
    }
    else
    {
        [notificationData setObject:items[index][@"email"] forKey:@"driverEmail"];
    }
    inSearchMode = NO;
    [self searchBarCancelButtonClicked:self.onlineSearchBar];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateFavouriteDriver" object:nil userInfo:notificationData];
}


#pragma mark - SearchBar Delegates
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    searchedData = [NSMutableArray array];
    [self.onlineTableView reloadData];
    keyWordForSearchBussiness = searchBar.text;
    [self filterContent:searchBar.text];
    //    indexPage = 0;
    //    [self getSearchBussiness:searchBar.text page:[NSString stringWithFormat:@"%d", indexPage]];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = nil;
    keyWordForSearchBussiness = nil;
    [searchBar resignFirstResponder];
    [searchedData removeAllObjects];
    inSearchMode = NO;
    [self.onlineTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(inSearchMode)
    {
        searchedData = [[NSMutableArray alloc] init];
    }
    keyWordForSearchBussiness = searchText;
    [self filterContent:searchText];
    //    indexPage = 0;
    //    [self getSearchBussiness:searchText page:[NSString stringWithFormat:@"%d", indexPage]];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if ([self.onlineSearchBar isUserInteractionEnabled])
    {
        inSearchMode = true;
        [searchBar setShowsCancelButton:inSearchMode animated:true];
        [self.onlineTableView reloadData];
    }
    else
    {
        [self.view endEditing:YES];
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    NSString *searchText = searchBar.text;
    // We're still 'in edit mode', if the user left a keyword in the searchBar
    inSearchMode = (searchText != nil && [searchText length] > 0);
    [searchBar setShowsCancelButton:inSearchMode animated:true];
    [self filterContent:searchText];
}

- (void)filterContent:(NSString*)searchText
{
    if(inSearchMode)
    {
        // Populate the results
        NSString *searchString = searchText;
        NSArray *filtered = [items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(name CONTAINS[c] %@)", searchString]];
        searchedData =[filtered mutableCopy];
        [self.onlineTableView reloadData];
    }
}

@end
