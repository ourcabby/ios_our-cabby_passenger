//
//  InviteViewController.h
//  UBER
//
//  Created by Rahul Sharma on 06/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WASWhatsAppUtil.h"

@interface InviteViewController : UIViewController<UIDocumentInteractionControllerDelegate,UIActionSheetDelegate>

- (IBAction)whatsupAction:(id)sender;
- (IBAction)telegramAction:(id)sender;



@property (weak, nonatomic) IBOutlet UILabel *whatsLabel;
@property (weak, nonatomic) IBOutlet UILabel *telegramLabel;

@end
