//
//  OfflineTableViewCell.h
//  Tapp4
//
//  Created by Rahul Sharma on 05/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView/AXRatingView.h>

@protocol favDriverOfflineDelegate <NSObject>
@optional
-(void) addFavouriteBtnDelegate:(NSDictionary *)delegateData;
@end


@interface OfflineTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *carDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIView *ratingview;
@property (strong, nonatomic) UIActivityIndicatorView * activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *addFavouriteBtn;
@property(nonatomic,strong)AXRatingView *ratingViewStars;
@property (nonatomic,assign) id<favDriverOfflineDelegate> delegate;


- (IBAction)addFavouriteBtn:(id)sender;

-(void)loadLabelData:(NSDictionary *) drvireDetails index:(NSIndexPath *)indexPath;
@end
