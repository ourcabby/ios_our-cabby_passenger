//
//  EmptyCell.h
//  RoadyoDispatch
//
//  Created by 3Embed on 03/06/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *emptyImageView;
@property (weak, nonatomic) IBOutlet UILabel *emptyStatuslabel;

@end
