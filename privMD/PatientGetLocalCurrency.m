//
//  PatientGetLocalCurrency.m
//  UBER
//
//  Created by Rahul Sharma on 14/08/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PatientGetLocalCurrency.h"

@implementation PatientGetLocalCurrency


+(NSString *)getCurrencyLocal:(float)amount
{

    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setLocale:[NSLocale currentLocale]];
    [currencyFormatter setMaximumFractionDigits:0];
    [currencyFormatter setMinimumFractionDigits:0];
    [currencyFormatter setAlwaysShowsDecimalSeparator:NO];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    NSNumber *someAmount = [NSNumber numberWithFloat:amount];
    NSString *Currencystring = [currencyFormatter stringFromNumber:someAmount];
    return Currencystring;
}

+(NSString *)getCurrencyLocalWithInt:(int)amount
{
    
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setLocale:[NSLocale currentLocale]];
    [currencyFormatter setMaximumFractionDigits:0];
    [currencyFormatter setMinimumFractionDigits:0];
    [currencyFormatter setAlwaysShowsDecimalSeparator:NO];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    NSNumber *someAmount = [NSNumber numberWithInt:amount];
    NSString *Currencystring = [currencyFormatter stringFromNumber:someAmount];
    return Currencystring;
}

+(NSString *)getCurrencyLocalForTip:(float)amount
{
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setLocale:[NSLocale currentLocale]];
    [currencyFormatter setMaximumFractionDigits:0];
    [currencyFormatter setMinimumFractionDigits:0];
    [currencyFormatter setAlwaysShowsDecimalSeparator:NO];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *someAmount = [NSNumber numberWithFloat:amount];
    // NSString *str = NSLocalizedString(@"EGP", @"EGP");
    NSString *Currencystring =[currencyFormatter stringFromNumber:someAmount];//[NSString stringWithFormat:@"%@ %@", str, someAmount];
    return Currencystring;
}

+(NSString *)getCurrencyLocalForWallet:(int)amount
{
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setLocale:[NSLocale currentLocale]];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString *Currencystring =[currencyFormatter stringFromNumber:@""];//[NSString stringWithFormat:@"%@",currencyFormatter];
    return Currencystring;
}


@end
