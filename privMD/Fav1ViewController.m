//
//  Fav1ViewController.m
//  Tapp4
//
//  Created by Rahul Sharma on 05/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "Fav1ViewController.h"
#import "CustomNavigationBar.h"
#import "XDKAirMenuController.h"
#import "OnlineViewController.h"
#import "OfflineViewController.h"
#import "Favourite1ViewController.h"

@interface Fav1ViewController ()<CustomNavigationBarDelegate, UIScrollViewDelegate>
{
    NSDictionary *itemList;
    NSMutableArray *onLineItems;
    NSMutableArray *offLineItems;
    NSMutableArray *favDriverItems;
    float latitude;
    float longitude;
    
    OnlineViewController *onlineVC ;
    OfflineViewController *offlineVC;
    Favourite1ViewController *favDriverVC;
    
    NSInteger index;
    NSInteger iscomingFrom;
}
@end

@implementation Fav1ViewController
@synthesize containerScrollView;
@synthesize onlineBtn, offlineBtn, favouriteBtn;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addCustomNavigationBar];
    itemList = [[NSMutableDictionary alloc] init];
    onLineItems = [[NSMutableArray alloc] init];
    offLineItems = [[NSMutableArray alloc] init];
    favDriverItems = [[NSMutableArray alloc] init];
    
    [containerScrollView setContentSize:CGSizeMake(960,self.view.frame.size.height-158)];
    containerScrollView.scrollEnabled = YES;
    containerScrollView.delegate = self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updaeDriver:)
                                                 name:@"updateFavouriteDriver"
                                               object:nil];
}

-(void)updaeDriver:(NSNotification *) userInfo
{
    if ([userInfo.userInfo[@"isComingFrom"] integerValue] == 1)
    {
        iscomingFrom = 1;
    }
    else if ([userInfo.userInfo[@"isComingFrom"] integerValue] == 2)
    {
        iscomingFrom = 2;
    }
    else if ([userInfo.userInfo[@"isComingFrom"] integerValue] == 3)
    {
        iscomingFrom = 3;
    }
    NSString *email = userInfo.userInfo[@"driverEmail"];
    if ([userInfo.userInfo[@"addOrRemove"] integerValue] == 1)
    {
        [self sendRequestToAddFavDriver:email];
    }
    else
    {
        [self sendRequestToRemoveDriverFromFavourites:email];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    PMDReachabilityWrapper *reachable = [PMDReachabilityWrapper sharedInstance];
    if ([reachable isNetworkAvailable] == YES)
    {
        [self sendRequestToGetFavouritesDriver];
    }
    else
    {
        [[ProgressIndicator sharedInstance]showMessage:NSLocalizedString(@"No Internet Connection", @"No Internet Connection") On:self.view];
        [self onlineBtnClick:self];
    }
}


#pragma mark- Custom Navigation Methods
/*--------------------------------------------------------------------------------------------*/
- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"FAVORITE DRIVERS", @"FAVORITE DRIVERS")];
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    [self menuButtonclicked];
}

- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
    {
        [menu closeMenuAnimated];
    }
    else
    {
        [self.view endEditing:YES];
        [menu openMenuAnimated];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




-(void)sendRequestToGetFavouritesDriver
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..", @"Loading..")];
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *currentDate = [Helper getCurrentDateTime];
    latitude = [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLat]floatValue];
    longitude = [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLong]floatValue];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_date_time":currentDate,
                             @"ent_longitude":[NSNumber numberWithFloat:longitude],
                             @"ent_latitude":[NSNumber numberWithFloat:latitude],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getFavourites"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       NSLog(@"response %@",response);
                                       onLineItems = [[NSMutableArray alloc] init];
                                       offLineItems = [[NSMutableArray alloc] init];
                                       favDriverItems = [[NSMutableArray alloc] init];
                                       [self parseToGetFavouritesDriverResponse:response];
                                   }
                               }];
}

-(void)parseToGetFavouritesDriverResponse:(NSDictionary*)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (response == nil)
    {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            if([response[@"mastersOnline"] count])
                [onLineItems addObjectsFromArray:response[@"mastersOnline"]];
            if([response[@"mastersOffline"] count])
                [offLineItems addObjectsFromArray:response[@"mastersOffline"]];
            NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:onLineItems];
            [arr addObjectsFromArray:offLineItems];
            
            for (int i = 0; i< arr.count; i++)
            {
                if ([arr[i][@"fav"] integerValue])
                {
                    [favDriverItems addObject:arr[i]];
                }
            }
            if (iscomingFrom == 1)
            {
                [self onlineBtnClick:self];
            }
            else  if (iscomingFrom == 2)
            {
                [self offlineBtnClick:self];
            }
            else  if (iscomingFrom == 3)
            {
                [self favDriverBtnClick:self];
            }
            else
            {
                [self onlineBtnClick:self];
            }
        }
        else if ([[response objectForKey:@"errFlag"] integerValue] == 1)
        {
            // [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"errMsg"]];
        }
    }
}

-(void)sendRequestToAddFavDriver:(NSString *) email
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Adding...", @"Adding...")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_dri_email":email,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    
    NSLog(@"Add Favourite Drivers Params = %@", params);
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"addTofavourite"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [pi hideProgressIndicator];
                                       [self sendRequestToGetFavouritesDriver];
                                       NSLog(@"response %@",response);
                                       NSLog(@"Add Favourite Drivers response = %@", response);

//                                       if (iscomingFrom == 1)
//                                       {
//                                           [onLineItems removeObjectAtIndex:index-1];
//                                           [onLineItems insertObject:updatedArray atIndex:index-1];
//                                           [favDriverItems addObject:updatedArray];
//                                           NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"dis"
//                                                                                                        ascending:YES];
//                                           NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
//                                           NSArray *sortedArray = [onLineItems sortedArrayUsingDescriptors:sortDescriptors];
//                                           onLineItems = [sortedArray mutableCopy];
//                                       }
//                                       else if (iscomingFrom ==2)
//                                       {
//                                           [offLineItems removeObjectAtIndex:index-1];
//                                           [offLineItems insertObject:updatedArray atIndex:index-1];
//                                           [favDriverItems addObject:updatedArray];
//                                           NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"dis"
//                                                                                                        ascending:YES];
//                                           NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
//                                           NSArray *sortedArray = [offLineItems sortedArrayUsingDescriptors:sortDescriptors];
//                                           favDriverItems = [sortedArray mutableCopy];
//                                       }
//                                       else if (iscomingFrom == 3)
//                                       {
//                                           [offLineItems removeObjectAtIndex:index-1];
//                                           [offLineItems insertObject:updatedArray atIndex:index-1];
////                                           NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"dis"
////                                                                                                        ascending:YES];
////                                           NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
////                                           NSArray *sortedArray = [favDriverItems sortedArrayUsingDescriptors:sortDescriptors];
////                                           favDriverItems = [sortedArray mutableCopy];
//                                       }
                                   }
                               }];
}




-(void)sendRequestToRemoveDriverFromFavourites:(NSString *)driverEmail
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Removing...", @"Removing...")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_dri_email":driverEmail,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    
    NSLog(@"Remove Favourite Drivers Params = %@", params);
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"removeFavourites"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   NSLog(@"Remove Favourite Drivers response = %@", response);
                                   if (success)
                                   {
                                       //handle success response
                                       NSLog(@"response %@",response);
                                       [self parseRemoveDriverFromFavouritesResponse:response];
                                   }
                               }];
}

-(void)parseRemoveDriverFromFavouritesResponse:(NSDictionary*)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (response == nil)
    {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
    }
    else
    {
        [self sendRequestToGetFavouritesDriver];
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            NSLog(@"Response = %@", response);
        }
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
   // CGPoint tableScrollPosition = [scrollView.panGestureRecognizer velocityInView:containerScrollView];
    
    CGFloat scrollViewOffsetX = scrollView.contentOffset.x;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if (scrollViewOffsetX < screenWidth/2)
    {
        [self onlineBtnClick:self];
    }
    else if (scrollViewOffsetX > screenWidth/2 && scrollViewOffsetX < screenWidth*2-screenWidth/2 )
    {
        [self offlineBtnClick:self];
    }
    else if ( scrollViewOffsetX > screenWidth*2-screenWidth/2)
    {
        [self favDriverBtnClick:self];
    }
}

- (IBAction)onlineBtnClick:(id)sender
{
    [self.view endEditing:YES];
    CGRect frame = containerScrollView.bounds;
    frame.origin.x = 0;
    [containerScrollView scrollRectToVisible:frame animated:YES];
    [onlineBtn setTitleColor:UIColorFromRGB(0x4688F1) forState:UIControlStateNormal];
    [offlineBtn setTitleColor:UIColorFromRGB(0x5b6570) forState:UIControlStateNormal];
    [favouriteBtn setTitleColor:UIColorFromRGB(0x5b6570) forState:UIControlStateNormal];
    [onlineBtn setBackgroundImage:[UIImage imageNamed:@"home_nearby_btn_selector.png"] forState:UIControlStateNormal];
    [offlineBtn setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    [favouriteBtn setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    [onlineVC getDataFromParentClass1:onLineItems];
}

- (IBAction)offlineBtnClick:(id)sender
{
    [self.view endEditing:YES];
    CGRect frame = containerScrollView.bounds;
    frame.origin.x = 320;
    [containerScrollView scrollRectToVisible:frame animated:YES];
    [onlineBtn setTitleColor:UIColorFromRGB(0x5b6570) forState:UIControlStateNormal];
    [offlineBtn setTitleColor:UIColorFromRGB(0x4688F1) forState:UIControlStateNormal];
    [favouriteBtn setTitleColor:UIColorFromRGB(0x5b6570) forState:UIControlStateNormal];
    [onlineBtn setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    [offlineBtn setBackgroundImage:[UIImage imageNamed:@"home_nearby_btn_selector.png"] forState:UIControlStateNormal];
    [favouriteBtn setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    [offlineVC getDataFromParentClass2:offLineItems];
}

- (IBAction)favDriverBtnClick:(id)sender
{
    [self.view endEditing:YES];
    CGRect frame = containerScrollView.bounds;
    frame.origin.x = 640;
    [containerScrollView scrollRectToVisible:frame animated:YES];
    [onlineBtn setTitleColor:UIColorFromRGB(0x5b6570) forState:UIControlStateNormal];
    [offlineBtn setTitleColor:UIColorFromRGB(0x5b6570) forState:UIControlStateNormal];
    [favouriteBtn setTitleColor:UIColorFromRGB(0x4688F1) forState:UIControlStateNormal];
    [onlineBtn setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    [offlineBtn setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    [favouriteBtn setBackgroundImage:[UIImage imageNamed:@"home_nearby_btn_selector.png"] forState:UIControlStateNormal];
    [favDriverVC getDataFromParentClass3:favDriverItems];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSDictionary *)sender
{
    if([[segue identifier] isEqualToString:@"onlineVCSegue"])
    {
        onlineVC = [segue destinationViewController];
    }
    else if([[segue identifier] isEqualToString:@"offlineVCSegue"])
    {
        offlineVC = [segue destinationViewController];
    }
    else if([[segue identifier] isEqualToString:@"favDriverVCSegue"])
    {
        favDriverVC = [segue destinationViewController];
    }
}

@end
