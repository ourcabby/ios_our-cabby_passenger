//
//  SignInViewController.h
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginManagerLoginResult.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginButton.h>
#import "FBLoginHandler.h"



@interface SignInViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,FBLoginHandlerDelegate>
{
    NSDictionary *itemList;
    BOOL checkLoginCredentials;
}

/**
 *  storing the data from facebook
 */
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (nonatomic,strong) NSDictionary *fbData;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (strong, nonatomic) IBOutlet UIView *containEmailnPass;
@property (weak, nonatomic) IBOutlet UILabel *fNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UIButton *signinButton;
@property (weak, nonatomic) IBOutlet UIImageView *countryImageView;
@property (weak, nonatomic) IBOutlet UIButton *countryCodeButton;

@property (strong, nonatomic)  UIButton *navDoneButton;
@property (nonatomic)  BOOL facebookDataValidation;
@property (strong, nonatomic) UIImageView *emailImageView;

- (IBAction)forgotPasswordButtonClicked:(id)sender;

- (IBAction)signInButtonClicked:(id)sender;

- (IBAction)facebookSingInAction:(id)sender;

- (IBAction)googleSigninButtonAction:(id)sender;

- (BOOL)myMobileNumberValidate:(NSString*)number;
- (IBAction)countryCodeButton:(id)sender;

@end
