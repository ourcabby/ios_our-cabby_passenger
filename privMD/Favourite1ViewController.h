//
//  CurrentOrderViewController.h
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 12/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Favourite1ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *favDriverTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *favDriverSearchBar;
@property (strong, nonatomic) NSArray *items;

-(void)getDataFromParentClass3:(NSArray *)items;
@end
