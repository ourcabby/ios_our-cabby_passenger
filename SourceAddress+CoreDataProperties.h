//
//  SourceAddress+CoreDataProperties.h
//  RoadyoDispatch
//
//  Created by Rahul Sharma on 20/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SourceAddress.h"

NS_ASSUME_NONNULL_BEGIN

@interface SourceAddress (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *srcAddress;
@property (nullable, nonatomic, retain) NSString *srcAddress2;
@property (nullable, nonatomic, retain) NSNumber *srcLatitude;
@property (nullable, nonatomic, retain) NSNumber *srcLongitude;
@property (nullable, nonatomic, retain) NSString *keyId;
@property (nullable, nonatomic, retain) NSString *zipCode;

@end

NS_ASSUME_NONNULL_END
