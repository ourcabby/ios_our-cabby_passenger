//
//  ImogramHandler.h
//  RoadyoDispatch
//
//  Created by Rahul Sharma on 21/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  <UIKit/UIKit.h>

@interface ImogramHandler : NSObject
typedef enum {
    ImoImageType2 = 0,
    ImoAudioType,
    ImoVideoType
} Imo;
+ (id)getInstance;
- (void)sendText4:(NSString*)message;
- (void)sendFile2:(NSData *)data UTI:(Imo)type inView:(UIView *)view;

@end
