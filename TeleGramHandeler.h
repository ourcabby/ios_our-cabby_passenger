//
//  TeleGramHandeler.h
//  SharingExample
//
//  Created by Wagner Sales on 18/02/15.
//  Copyright (c) 2015 Wagner Sales. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  <UIKit/UIKit.h>

@interface TeleGramHandeler : NSObject

// TeleGram UTI
typedef enum {
	TeleGramImageType = 0,
	TeleGramAudioType,
	TeleGramVideoType
} TeleGram;

+ (id)getInstance;
- (void)sendText1:(NSString*)message;
- (void)sendFile1:(NSData *)data UTI:(TeleGram)type inView:(UIView *)view;

@end
