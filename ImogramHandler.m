//
//  ImogramHandler.m
//  RoadyoDispatch
//
//  Created by Rahul Sharma on 21/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ImogramHandler.h"

NSString *const ImoUrl = @"imo://app";
NSString *const ImoSendTextUrl = @"imo://msg?text=";

NSString *UTIWithImoType(Imo type) {
    NSArray *arr = @[
                     @"com.imo.image", //image
                     @"com.imo.audio", //audio
                     @"com.imo.movie"  //movie
                     ];
    return (NSString *)[arr objectAtIndex:type];
}

NSString *typeWithImoType(Imo type) {
    NSArray *arr = @[
                     @"ImoTmp.wai", //image
                     @"ImoTmp.waa", //audio
                     @"ImoTmp.wam"  //movie
                     ];
    return (NSString *)[arr objectAtIndex:type];
}
// Instace
__strong static ImogramHandler* instanceOf = nil;

@interface ImogramHandler()<UIDocumentInteractionControllerDelegate>{
    UIDocumentInteractionController *_docControll;
}

@end

@implementation ImogramHandler

#pragma mark - Instance
+ (ImogramHandler*)getInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instanceOf = [[ImogramHandler alloc] init];
    });
    return instanceOf;
}

#pragma mark - Sends
- (void)sendText4:(NSString *)message
{
    NSString *urlWhats = [NSString stringWithFormat:@"%@%@",ImoSendTextUrl,message];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    urlWhats = [urlWhats stringByAddingPercentEncodingWithAllowedCharacters:set];
    NSURL *ImoURL = [NSURL URLWithString:urlWhats];
    
    if ( [self isImoInstalled] ) {
        [[UIApplication sharedApplication] openURL:ImoURL];
    } else {
        [self alertImoNotInstalled];
    }
}

- (void)sendFile2:(NSData *)data UTI:(Imo )type inView:(UIView *)view
{
    if ( [self isImoInstalled] )
    {
        NSURL *tempFile	= [self createTempFile:data type:typeWithImoType(type)];
        _docControll = [UIDocumentInteractionController interactionControllerWithURL:tempFile];
        _docControll.UTI = UTIWithImoType(type);
        _docControll.delegate = self;
        
        [_docControll presentOpenInMenuFromRect:CGRectZero
                                         inView:view
                                       animated:YES];
    } else {
        [self alertImoNotInstalled];
    }
}

#pragma mark - Helpers
- (NSURL *)createTempFile:(NSData *)data type:(NSString *)type
{
    NSError *error = nil;
    NSURL *tempFile = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory
                                                             inDomain:NSUserDomainMask
                                                    appropriateForURL:nil
                                                               create:NO
                                                                error:&error];
    
    if (tempFile)
    {
        tempFile = [tempFile URLByAppendingPathComponent:type];
    } else {
        [self alertErro:[NSString stringWithFormat:@"Error getting document directory: %@", error]];
    }
    
    if (![data writeToURL:tempFile options:NSDataWritingAtomic error:&error]){
        [self alertErro:[NSString stringWithFormat:@"Error writing File: %@", error]];
    }
    
    return tempFile;
}

- (BOOL)isImoInstalled
{
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:ImoUrl]];
}

- (void)alertWithTitle:(NSString *)title message:(NSString *)message
{
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                              style:UIAlertActionStyleCancel
                                            handler:^(UIAlertAction *action) {
                                                
                                                [vc dismissViewControllerAnimated:YES completion:^{}];
                                            }]];
    
    [vc presentViewController:alert animated:YES completion:nil];
}

- (void)alertImoNotInstalled
{
    [self alertErro:@"Your device has no Imo installed."];
}

- (void)alertErro:(NSString *)message
{
    [self alertWithTitle:@"Error" message:message];
}

@end
