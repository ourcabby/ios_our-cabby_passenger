//
//  WASTeleGramUtil.m
//  SharingExample
//
//  Created by Wagner Sales on 18/02/15.
//  Copyright (c) 2015 Wagner Sales. All rights reserved.
//

#import "TeleGramHandeler.h"

// TeleGram URLs
NSString *const TeleGramUrl = @"telegram://app";
NSString *const TeleGramSendTextUrl = @"telegram://msg?text=";

// TeleGram UTI
NSString *UTIWithTeleGramType(TeleGram type) {
	NSArray *arr = @[
					 @"com.telegram.image", //image
					 @"com.telegram.audio", //audio
					 @"com.telegram.movie"  //movie
					 ];
	return (NSString *)[arr objectAtIndex:type];
}

NSString *typeWithTeleGramType(TeleGram type) {
	NSArray *arr = @[
					 @"TelegramTmp.wai", //image
					 @"TelegramTmp.waa", //audio
					 @"TelegramTmp.wam"  //movie
					 ];
	return (NSString *)[arr objectAtIndex:type];
}

// Instace
__strong static TeleGramHandeler* instanceOf = nil;

@interface TeleGramHandeler()<UIDocumentInteractionControllerDelegate>{
	UIDocumentInteractionController *_docControll;
}

@end

@implementation TeleGramHandeler

#pragma mark - Instance
+ (TeleGramHandeler*)getInstance
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		instanceOf = [[TeleGramHandeler alloc] init];
	});
	return instanceOf;
}

#pragma mark - Sends

- (void)sendText1:(NSString *)message
{
	NSString *urlWhats = [NSString stringWithFormat:@"%@%@",TeleGramSendTextUrl,message];
	NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
	urlWhats = [urlWhats stringByAddingPercentEncodingWithAllowedCharacters:set];
	NSURL *teleGramURL = [NSURL URLWithString:urlWhats];
	
	if ( [self isTeleGramInstalled] ) {
		[[UIApplication sharedApplication] openURL:teleGramURL];
	} else {
		[self alertTeleGramNotInstalled];
	}
}

- (void)sendFile1:(NSData *)data UTI:(TeleGram )type inView:(UIView *)view
{
	if ( [self isTeleGramInstalled] )
	{
		NSURL *tempFile	= [self createTempFile:data type:typeWithTeleGramType(type)];
		_docControll = [UIDocumentInteractionController interactionControllerWithURL:tempFile];
		_docControll.UTI = UTIWithTeleGramType(type);
		_docControll.delegate = self;
		
		[_docControll presentOpenInMenuFromRect:CGRectZero
										 inView:view
									   animated:YES];
	} else {
		[self alertTeleGramNotInstalled];
	}
}

#pragma mark - Helpers
- (NSURL *)createTempFile:(NSData *)data type:(NSString *)type
{
	NSError *error = nil;
	NSURL *tempFile = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory
																inDomain:NSUserDomainMask
													appropriateForURL:nil
																  create:NO
																error:&error];
	
	if (tempFile)
	{
		tempFile = [tempFile URLByAppendingPathComponent:type];
	} else {
		[self alertErro:[NSString stringWithFormat:@"Error getting document directory: %@", error]];
	}
	
	if (![data writeToURL:tempFile options:NSDataWritingAtomic error:&error]){
		[self alertErro:[NSString stringWithFormat:@"Error writing File: %@", error]];
	}
	
	return tempFile;
}

- (BOOL)isTeleGramInstalled
{
	return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:TeleGramUrl]];
}

- (void)alertWithTitle:(NSString *)title message:(NSString *)message
{
	UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
																   message:message
															preferredStyle:UIAlertControllerStyleAlert];
	
	[alert addAction:[UIAlertAction actionWithTitle:@"OK"
										   style:UIAlertActionStyleCancel
										 handler:^(UIAlertAction *action) {
											 
		 [vc dismissViewControllerAnimated:YES completion:^{}];
	 }]];
	
	[vc presentViewController:alert animated:YES completion:nil];
}

- (void)alertTeleGramNotInstalled
{
	[self alertErro:@"Your device has no TeleGram installed."];
}

- (void)alertErro:(NSString *)message
{
	[self alertWithTitle:@"Error" message:message];
}

@end
