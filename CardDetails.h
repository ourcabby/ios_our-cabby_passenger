//
//  CardDetails.h
//  RoadyoDispatch
//
//  Created by 3Embed on 18/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>


@interface CardDetails : NSManagedObject

@property (nonatomic, retain) NSString * expMonth;
@property (nonatomic, retain) NSString * expYear;
@property (nonatomic, retain) NSString * idCard;
@property (nonatomic, retain) NSString * last4;
@property (nonatomic, retain) NSString * cardtype;
@property (nonatomic, retain) NSString * isDefault;

@end
